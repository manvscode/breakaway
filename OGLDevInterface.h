#pragma once
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

class OGLDevInterface
{
	HINSTANCE hInstance;
	HWND hWnd;
	HDC hDC;
	HGLRC hRC;
	GLsizei width, height, BitsPerPixel;
	bool fullscreen;

public:
	OGLDevInterface(void);
	~OGLDevInterface(void);

	GLvoid ReSizeGLScene(GLsizei width, GLsizei height);
	int InitGL(GLvoid);
	int DrawGLScene(GLvoid);
	bool CreateGLWindow(char *title, int width, int height, int bits, bool fullscreenflag);
	GLvoid KillGLWindow(GLvoid);
	GLvoid GoFullscreen(GLvoid);
	GLvoid ToggleFullscreen(GLvoid);
	static LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);


};
