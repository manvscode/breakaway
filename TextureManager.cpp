/*	TextureManager.cpp
 *
 *	Manages game textures.
 *	Coded by Joseph A. Marrero
 */
#include "texturemanager.h"
#include "ImageIO.h"
#include <assert.h>
#include ".\texturemanager.h"

TextureManager::TextureManager(void)
{


	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
}

TextureManager::~TextureManager(void)
{


}
bool TextureManager::AddTexture(char *filename, unsigned int Texture_Type)
{
	switch(Texture_Type){
		case BMP_TEXTURE:
			return true;
			break;
		case TGA_TEXTURE:
				AddTGATexture(filename, Texture_Type);
				return true;
			
			break;
//		default:
		//	return;
		//	break;
	}
	return false;

}
bool TextureManager::AddTGATexture(char *filename, unsigned int Texture_Type = 2)
{
	Texture *newTexture = NULL;
	TGAFILE *newTGAFile = NULL;

	newTGAFile = new TGAFILE;
	newTexture = new Texture;

	if( newTGAFile == NULL || newTexture == NULL) 
		return false;
	else {
		if( !LoadTGAFile(filename, newTGAFile) ){
			MessageBox(NULL, "There was a problem loading a targa texture!", "Error", MB_OK |MB_ICONERROR);
			return false;
		}
		//newTexture->TextureType = 2;		// 2D textures!!!!
		newTexture->TextureType = Texture_Type;
		newTexture->target = GL_TEXTURE_2D;
		newTexture->level = 0;
		newTexture->internalFormat = GL_RGBA; 
		newTexture->width = (GLsizei) newTGAFile->imageWidth;
		newTexture->height = (GLsizei) newTGAFile->imageHeight;
		newTexture->border = 0;
		//newTexture->format = (newTGAFile->bitCount / 8 == 3) ? GL_RGB : GL_RGBA; // 3 = RGB, 4 RGBA
		newTexture->format = (newTGAFile->bitCount >> 3 == 3) ? GL_RGB : GL_RGBA; // 3 = RGB, 4 RGBA
		newTexture->type = GL_UNSIGNED_BYTE;
		newTexture->texels = (GLvoid *) newTGAFile->imageData;
		newTexture->textureSrc = (void *) newTGAFile;
		newTexture->textureSrcSize = sizeof(TGAFILE);
		newTexture->textureFormatType = TGA_TEXTURE;
				
		glGenTextures(1, &newTexture->TextureName);		//Generate one unique Texture Name
		assert(!glGetError());
		TextureList.push_back( newTexture );

		/*
		 *		Set Texture up
		 */
		glBindTexture(GL_TEXTURE_2D, newTexture->TextureName);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glTexImage2D(GL_TEXTURE_2D, newTexture->level, newTexture->internalFormat, newTexture->width, newTexture->height, newTexture->border, newTexture->format, newTexture->type, newTexture->texels);
		assert(!glGetError());

	}
	return true;
}

void TextureManager::RemoveTGATexture(int textureName)
{
	list<Texture *>::iterator itr;

	for( itr = TextureList.begin(); itr != TextureList.end(); ++itr )
		if( (*itr)->TextureName == textureName )
		{
			DestroyTexture( (*itr)->texels );
			TextureList.erase( itr );
			return;
		}
}
Texture *TextureManager::GetTGATexture(int textureName)
{
	list<Texture *>::iterator itr;

	for( itr = TextureList.begin(); itr != TextureList.end(); ++itr )
		if( (*itr)->TextureName == textureName )
			return (*itr);
	
	return NULL;
}
void TextureManager::DestroyTGATextureList()
{
	list<Texture *>::iterator itr;

	for( itr = TextureList.begin(); itr != TextureList.end(); ++itr )
	{
		free( ((TGAFILE *) (*itr)->textureSrc)->imageData ); //Free the malloc image data
		delete (*itr)->textureSrc;   // Deletes TGAFILE Struct
		delete *itr;
		TextureList.erase( itr++ ); // this does not invalidate the itr
	}
}
/*
 *	DestroyTexture() needed to destroy linked list texture nodes.
 */
void DestroyTexture(void *data)
{
	Texture *texture = (Texture *) data;
	if( texture->textureFormatType == TGA_TEXTURE){
		glDeleteTextures(1, &texture->TextureName); 
		DestroyTGAImageData((TGAFILE *) texture->textureSrc);
		delete (TGAFILE *) ((Texture *) data)->textureSrc;
	}
	else if(((Texture *) data)->textureFormatType == BMP_TEXTURE){
		glDeleteTextures(1, &texture->TextureName); 
		delete (BITMAPINFOHEADER *) ((Texture *) data)->textureSrc;
	}


	delete (Texture *) data;
}

