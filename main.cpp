#pragma once
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glaux.h>
#include "Globals.h"
#include "Game.h"
#include "resource.h"

void SetupPixelDescriptor(HDC hDC);
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
bool LockFrameRate(int frame_rate);

void SetupPixelDescriptor(HDC hDC)
{
	int PixelFormat;

	static PIXELFORMATDESCRIPTOR pfd = {
		sizeof(PIXELFORMATDESCRIPTOR),
			1,
			PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_GENERIC_ACCELERATED|
			PFD_DOUBLEBUFFER, PFD_TYPE_RGBA,
			32,
			0, 0, 0, 0, 0, 0,
			0,
			0,
			0,
			0, 0, 0, 0,
			16,
			0,
			0,
			PFD_MAIN_PLANE,
			0,
			0, 0, 0
	};

	PixelFormat = ChoosePixelFormat(hDC, &pfd);
	SetPixelFormat(hDC, PixelFormat, &pfd);
}
bool GoFullscreen(int ScreenWidth, int ScreenHeight, int ScreenBpp)
{
	DEVMODE devModeScreen;
	memset(&devModeScreen, 0, sizeof(devModeScreen));
	devModeScreen.dmSize = sizeof(devModeScreen);
	devModeScreen.dmPelsWidth = ScreenWidth;
	devModeScreen.dmPelsHeight = ScreenHeight;
	devModeScreen.dmBitsPerPel = ScreenBpp;
	devModeScreen.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

	if( ChangeDisplaySettings(&devModeScreen, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL){
		fullscreen = false;
		return false;
	}

	return true;
}
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	WNDCLASSEX	wndClass;
	HWND hWnd;
	MSG msg;
	bool done = false;
	RECT windowRect;
	DWORD windowStyle, extendedWindowStyle;


	windowRect.top =0;
	windowRect.left =0;
	windowRect.bottom = 600;
	windowRect.right = 800;


	wndClass.cbSize = sizeof( WNDCLASSEX );
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc = WndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	//wndClass.hIcon = LoadIcon(NULL, IDI_ICON);
	wndClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON));
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = "Pong";
	//wndClass.hIconSm = LoadIcon(hInstance, IDI_ICON);
	wndClass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON));

	if( !RegisterClassEx(&wndClass) ){
		MessageBox(NULL,"Unable to register window class!", "Error", MB_OK |MB_ICONERROR);
		return -1;
	}

	hWnd = CreateWindowEx(NULL, "Pong", "::: Pong :::",WS_POPUP | WS_EX_TOPMOST, 0, 0, RESOLUTION_X, RESOLUTION_Y, NULL, NULL, hInstance, NULL);
	
	if(!hWnd){
		MessageBox(NULL,"Unable to create window!", "Error", MB_OK |MB_ICONERROR);
		return -1;
	}
	ShowWindow(hWnd, SW_SHOW);
	
	if(fullscreen){
		GoFullscreen(RESOLUTION_X, RESOLUTION_Y, BIT_RATE);

		extendedWindowStyle = WS_EX_APPWINDOW;
		windowStyle = WS_POPUP;
		ShowCursor(FALSE);
	}
	else {
		extendedWindowStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		windowStyle = WS_OVERLAPPEDWINDOW; //|	WS_VISIBLE | WS_SYSMENU | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	}
	
	AdjustWindowRectEx(&windowRect, windowStyle, FALSE, extendedWindowStyle);
	//ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
	

	while(!done){
		//PeekMessage(&msg, hWnd, NULL, NULL, PM_REMOVE);
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
				break;
				
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			PongGame->GameTimer->ResetTimer();
			

			
			PongGame->Render();

			if( PongGame->getTimeStep() < (1.0f / 100.0f) )
				Sleep( (DWORD) ((1.0f / 100.0f) - PongGame->getTimeStep()) );

			TranslateMessage(&msg);
			DispatchMessage(&msg);
			PongGame->setTimeStep( PongGame->GameTimer->GetTime() );
		}
	}


	return 0;
}
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT		ps;
	static HGLRC	hRC;
	static HDC		hDC;
	int				width,
					height;

	switch(message){
		case WM_CREATE:
			hDC = GetDC(hwnd);
			SetupPixelDescriptor(hDC);
			hRC = wglCreateContext(hDC);
			wglMakeCurrent(hDC, hRC);
			PongGame = PongGame->GetInstance();  //Starts Game Engine
			PongGame->SetHDC(hDC);
			PongGame->InitGame();   //Starts Managers
			return 0;
			break;
		case WM_PAINT:
			BeginPaint(hwnd, &ps);
			EndPaint(hwnd, &ps);
			return 0;
			break;
		case WM_CLOSE:
			ReleaseDC(hwnd, hDC);
			delete PongGame;
			wglMakeCurrent(hDC, NULL);
			wglDeleteContext(hRC);
			PostQuitMessage(0);
			return 0;
			break;
		case WM_DESTROY:
			ReleaseDC(hwnd, hDC);
			break;
		case WM_SIZE:
			height = HIWORD(lParam);
			width = LOWORD(lParam);
			
			if(height == 0)
				height = 1;

			glViewport(0,0,width, height);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0.0f, width - 1.0, 0.0, height - 1.0, -1.0, 1.0);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			return 0;
			break;
		case WM_KEYUP:						
		{
			PongGame->keys[wParam] = FALSE;				
			return 0;						
		}
		case WM_KEYDOWN:					
		{
			PongGame->keys[wParam] = TRUE;	

			if(PongGame->keys[VK_ESCAPE] == TRUE){		
				delete PongGame;
				wglMakeCurrent(hDC, NULL);
				wglDeleteContext(hRC);
				PostQuitMessage(0);
				return 0;
			}
				
				
			return 0;
			break;
		}

		case WM_SYSCOMMAND:						
			{
				switch (wParam)						
				{
					case SC_SCREENSAVE:				
					case SC_MONITORPOWER:				
					return 0;					
				}
			break;							
			}

		default:
			break;
	}
	return DefWindowProc(hwnd,message, wParam,lParam);
}
// Locks the frame rate at "frame_rate"
// Returns true when it's okay to draw, false otherwise
bool LockFrameRate(int frame_rate)
{
	static float lastTime = 0.0f;
	
	// Get current time in seconds (milliseconds * .001 = seconds)
	float currentTime = GetTickCount() * 0.001f; 

	// Get the elapsed time by subtracting the current time from the last time
	// If the desired frame rate amount of seconds has passed -- return true (ie Blit())
	if((currentTime - lastTime) > (1.0f / frame_rate))
	{
		// Reset the last time
		lastTime = currentTime;	
			return true;
	}

	return false;
}