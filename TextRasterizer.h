/*	TextRasterizer.h
 *
 *	Writes Text to a GL context.
 *	Coded by Joseph A. Marrero
 *
 */
#pragma once
#include <windows.h>
#include <gl/gl.h>

class TextRasterizer
{
private:
	unsigned int base;
	HDC m_HDC;
	HFONT hFont;
	unsigned int FontSize;
	
public:
	TextRasterizer(HDC hDC);
	~TextRasterizer(void);

	BOOL CreateBitmapFont(char *fontName, int fontSize);
	void PrintString(char *String);
	void PrintString(float x, float y, char *String);
	void ClearFont();
};
