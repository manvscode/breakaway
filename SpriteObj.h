#pragma once

class SpriteObj
{
public:
	int x, y;
	int width, height;
	int collide_width, collide_height;
	int collide_x_offset, collide_y_offset;

	SpriteObj(void)
	{
	}

	~SpriteObj(void)
	{
	}

	unsigned int getWidth( ) const { return width; }
	unsigned int getHeight( ) const { return height; }
};
