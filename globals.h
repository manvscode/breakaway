#pragma once

#include <windows.h>
#include "Game.h"
/*	
 *	Globals.h
 *
 *	Has all globals needed for all objects.
 *	Coded by Joseph A. Marrero
 */

//HDC g_hDC;
//bool keys[256];						// Array Used For The Keyboard Routine
//bool active = TRUE;					// Window Active Flag Set To TRUE By Default
#define BIT_RATE	32
#define RESOLUTION_X	800
#define RESOLUTION_Y	600
#ifdef _DEBUG
	bool fullscreen = FALSE;
#else
	bool fullscreen = TRUE;
#endif
Game *PongGame = NULL;
//#define __SHOW_FPS__


