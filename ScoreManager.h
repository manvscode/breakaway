#pragma once
#include "Game.h"

class ScoreManager
{
	friend class Game;
	friend class TextRasterizer;
private:
	Game *GameInstance;
	int score;
	TextRasterizer *out;
	char ScoreString[32];

public:
	ScoreManager(Game *game_Instance);
	~ScoreManager(void);
	void AdjustScore(int scoreAdj);
	void AdjustScore();
	void WriteScore();
	void ResetScore();
};
