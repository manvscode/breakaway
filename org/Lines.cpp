#include ".\lines.h"

Lines::Lines(void)
{
	slope = 0.0;
	y_intercept = 0.0;
}
Lines::Lines(int x1, int y1, int x2, int y2)
{
	slope = ((float) (y2 - y1))/ (x2 - x1);
	//y1 = m * x1 + b
	y_intercept = y1 - slope*x1;
}

Lines::~Lines(void)
{
}
void Lines::GetReflectedLine(Lines &Line, Lines &IncidentLine, Lines *refLine)
{
	float x, refSlope, refY_Intercept, tempy;

	x = (IncidentLine.slope - Line.slope) / (Line.y_intercept - IncidentLine.y_intercept);

	refSlope = 1.0 / (-IncidentLine.slope);

	tempy = IncidentLine.slope * x + IncidentLine.y_intercept;

	refY_Intercept= tempy - refSlope * x;

	refLine->slope = refSlope;
	refLine->y_intercept = refY_Intercept;
}