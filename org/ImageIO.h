#pragma once
#ifndef __IMAGEIO_H__
#define __IMAGEIO_H__
#ifdef __cplusplus
extern "C" {
#endif 

/*	ImageIO.h
 *
 *	Various Data Structures and Image loading routines.
 *	Coded by Joseph A. Marrero
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include <windows.h>

#define	BITMAP_ID	0x4D42

/*
 *	Windows Bitmap
 */
/*
typedef struct tagBITMAPFILEHEADER {
	WORD	bfType;						// Specifies File Type; Must be BM (0x4D42)
	DWORD	bfSize;						// Specifies the size in bytes of the bitmap
	WORD	bfReserved1;				// Reserved; Must be zero!
	WORD	bfReserved2;				// Reserved; Must be zero!
	DWORD	bfOffBits;					// Specifies the offset, in bytes, from the 
										// BITMAPFILEHEADER structure to the bitmap bits
} BITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER {
	DWORD	biSize;						// Specifies number of bytes required by the structure
	LONG	biWidth;					// Specifies the width of the bitmap, in pixels
	LONG	biHeight;					// Specifies the height of the bitmap, in pixels
	WORD	biPlanes;					// Specifies the number of color planes, must be 1
	WORD	biBitCount;					// Specifies the bits per pixel, must be 1, 4,
										// 8, 16, 24, or 32
	DWORD	biCompression;				// Specifies the type of compression
	DWORD	biSizeImage;				// Specifies the size of the image in bytes
	LONG	biXPelsPerMeter;			// Specifies the number of pixels per meter in x axis
	LONG	biYPelsPerMeter;			// Specifies the number of pixels per meter in y axis
	DWORD	biClrUsed;					// Specifies the number of colors used by the bitmap
	DWORD	biClrImportant;				// Specifies the number of colors that are important
} BITMAPINFOHEADER;
*/

unsigned char *LoadBitmapFile(char *filename, BITMAPINFOHEADER *bitmapInfoHeader)
{
	FILE				*filePtr;
	BITMAPFILEHEADER	bitmapFileHeader;
	unsigned char		*bitmapImage = NULL;	// bitmap image data
	register unsigned int		imageIdx = 0;
	unsigned char		tempRGB;				// swap variable

	if((filePtr = fopen(filename, "rb")) == NULL)
		return NULL;

	fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr);

	if( bitmapFileHeader.bfType != BITMAP_ID){
		fclose(filePtr);
		return NULL;
	}

	fread(bitmapInfoHeader,sizeof(BITMAPINFOHEADER), 1 , filePtr);
	fseek(filePtr, bitmapFileHeader.bfOffBits, SEEK_SET);
	
	bitmapImage = (unsigned char *) malloc( bitmapInfoHeader->biSizeImage );
	
	if(bitmapImage == NULL){
		free(bitmapImage);	
		fclose(filePtr);	
		return NULL;
	}

	fread(bitmapImage, 1, bitmapInfoHeader->biSizeImage, filePtr);

	if(bitmapImage == NULL){
		fclose(filePtr);
		return NULL;
	}

	for( imageIdx = 0; imageIdx < bitmapInfoHeader->biSizeImage; imageIdx += 3){
		tempRGB = bitmapImage[imageIdx];
		bitmapImage[imageIdx] = bitmapImage[imageIdx + 2];
		bitmapImage[imageIdx + 2] = tempRGB;
	}

	fclose(filePtr);
	return bitmapImage;
}
int WriteBitmapFile(char *filename, int width, int height, unsigned char *imageData)
{
	FILE				*filePtr;
	BITMAPFILEHEADER	bitmapFileHeader;
	BITMAPINFOHEADER	bitmapInfoHeader;
	unsigned int		imageIdx = 0;
	unsigned char		tempRGB;				// swap variable

	if( (filePtr = fopen(filename, "wb")) == NULL)
		return 0;

	// Define the BitmapFileHeader
	bitmapFileHeader.bfSize = sizeof(BITMAPFILEHEADER);
	bitmapFileHeader.bfType = BITMAP_ID;
	bitmapFileHeader.bfReserved1 = 0;
	bitmapFileHeader.bfReserved2 = 0;
	bitmapFileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	// Define the BitmapInfoHeader
	bitmapInfoHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitmapInfoHeader.biPlanes = 1;
	bitmapInfoHeader.biBitCount = 24;				// 24 Bit
	bitmapInfoHeader.biCompression = BI_RGB;		// No compression
	bitmapInfoHeader.biSizeImage = width * abs(height) * 3;  // w * h * (RGB bytes)
	bitmapInfoHeader.biXPelsPerMeter = 0;
	bitmapInfoHeader.biYPelsPerMeter = 0;
	bitmapInfoHeader.biClrUsed = 0;
	bitmapInfoHeader.biClrImportant = 0;
	bitmapInfoHeader.biWidth = width;
	bitmapInfoHeader.biHeight = height;

	// swap RGB to BGR
	for( imageIdx = 0; imageIdx < bitmapInfoHeader.biSizeImage; imageIdx += 3){
		tempRGB = imageData[imageIdx];
		imageData[imageIdx] = imageData[imageIdx + 2];
		imageData[imageIdx + 2] = tempRGB;
	}

	fwrite(&bitmapFileHeader, 1, sizeof(BITMAPFILEHEADER), filePtr);
	fwrite(&bitmapInfoHeader, 1, sizeof(BITMAPINFOHEADER), filePtr);
	fwrite(imageData, bitmapInfoHeader.biSizeImage, 1, filePtr);

	fclose(filePtr);
	return 1;
}
/*
 *	Targa
 */
typedef struct tagTARGAFILEHEADER{
	unsigned char imageIDLength;		// number of characters in identification field;
										// 0 denotes no identification is included.
	unsigned char colorMapType;			// type of color map; always 0
	unsigned char imageTypeCode;		// uncompressed RGB is 2;
										// uncompressed Grayscale is 3
	short int colorMapOrigin;			// origin of color map (lo-hi); always 0
	short int colorMapLength;			// length of color map (lo-hi); always 0
	short int colorMapEntrySize;		// color map entry size (lo-hi); always 0;
	short int imageXOrigin;				// x coordinate of lower-left corner of image
										// (lo-hi); always 0
	short int imageYOrigin;				// y coordinate of lower-left corner of image
										// (lo-hi); always 0
	short int imageWidth;				// width of image in pixels (lo-hi)
	short int imageHeight;				// height of image in pixels (lo-hi)
	unsigned char bitCount;				// number of bits; 16, 24, 32
	unsigned char imageDescriptor;		// 24 bit = 0x00; 32-bit = 0x08
} TARGAFILEHEADER;

typedef struct tagTGAFILE{
	unsigned char imageTypeCode;
	short int imageWidth;
	short int imageHeight;
	unsigned char bitCount;
	unsigned char *imageData;
}TGAFILE;
int LoadTGAFile(char *filename, TGAFILE *tgaFile)
{
	FILE			*filePtr;
	unsigned char	ucharBad;		// garbage unsigned char data
	short int		sintBad;		// garbage short int data
	long			imageSize;		// size of the Targa image
	int				colorMode;		// 4 for RGBA or 3 for RGB
	register long	imageIdx;
	register unsigned char	colorSwap;		// Swap variable

	if( (filePtr = fopen(filename, "rb")) == NULL)
		return 0;

	// read in first two bytes we don't need
	fread(&ucharBad, sizeof(unsigned char), 1 , filePtr);
	fread(&ucharBad, sizeof(unsigned char), 1 , filePtr);

	//read in image type
	fread( &tgaFile->imageTypeCode, sizeof(unsigned char), 1, filePtr);
	
	if( (tgaFile->imageTypeCode != 2) && (tgaFile->imageTypeCode != 3) ){
		fclose(filePtr);
		return 0;
	}

	// read 13 bytes of data we don't need
	fread( &sintBad, sizeof(short int), 1, filePtr);
	fread( &sintBad, sizeof(short int), 1, filePtr);
	fread( &ucharBad, sizeof(unsigned char), 1, filePtr);
	fread( &sintBad, sizeof(short int), 1, filePtr);
	fread( &sintBad, sizeof(short int), 1, filePtr);

	fread( &tgaFile->imageWidth, sizeof(short int), 1 , filePtr);
	fread( &tgaFile->imageHeight, sizeof(short int), 1, filePtr);
	fread( &tgaFile->bitCount, sizeof(unsigned char), 1, filePtr);
	

	//read in one byte of data we don't need
	fread(&ucharBad, sizeof(unsigned char), 1, filePtr);

	//colorMode-> 3 = BGR, 4 = BGRA
	colorMode = tgaFile->bitCount / 8;
	imageSize = tgaFile->imageWidth * tgaFile->imageHeight * colorMode;

	tgaFile->imageData = (unsigned char *) malloc(sizeof(unsigned char) * imageSize);
	
	if(tgaFile->imageData == NULL)
		return 0;

	fread(tgaFile->imageData, sizeof(unsigned char), imageSize, filePtr);

	// Swap BGR to RGB
	for(imageIdx = 0; imageIdx < imageSize; imageIdx += colorMode){
		colorSwap = tgaFile->imageData[imageIdx];
		tgaFile->imageData[imageIdx] = tgaFile->imageData[imageIdx + 2];
		tgaFile->imageData[imageIdx + 2] = colorSwap;
	}

	fclose(filePtr);
	return 1;
}
int WriteTGAFile(char *filename, short int width, short int height, unsigned char *imageData)
{
	//Implement later.
	return 1;
}
void DestroyTGAImageData(TGAFILE *tgaFile)
{
	free(tgaFile->imageData);
}


#ifdef __cplusplus
}
#endif
#endif