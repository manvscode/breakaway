/*	TextureManager.cpp
 *
 *	Manages game textures.
 *	Coded by Joseph A. Marrero
 */
#include "texturemanager.h"
#include "ImageIO.h"
#include <assert.h>
#include ".\texturemanager.h"

TextureManager::TextureManager(void)
{
	TextureList = new DList(DestroyTexture);


	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
}

TextureManager::~TextureManager(void)
{
	/*
	switch(TextureType){
		case BMP_TEXTURE:
			break;
		case TGA_TEXTURE:
			//DestroyTGATextureList();
			break;
		default:
			break;
	}
	*/
	delete TextureList;		// Will call TextureList->DestroyList() in ~TextureList()

}
bool TextureManager::AddTexture(char *filename, unsigned int Texture_Type)
{
	switch(Texture_Type){
		case BMP_TEXTURE:
			return true;
			break;
		case TGA_TEXTURE:
				AddTGATexture(filename, Texture_Type);
				return true;
			
			break;
//		default:
		//	return;
		//	break;
	}
	return false;

}
bool TextureManager::AddTGATexture(char *filename, unsigned int Texture_Type = 2)
{
	Node *new_node = NULL;
	Texture *newTexture = NULL;
	TGAFILE *newTGAFile = NULL;

	new_node = new Node;
	newTGAFile = new TGAFILE;
	newTexture = new Texture;

	if( new_node == NULL || newTGAFile == NULL || newTexture == NULL) 
		return false;
	else {
		if( !LoadTGAFile(filename, newTGAFile) ){
			MessageBox(NULL, "There was a problem loading a targa texture!", "Error", MB_OK |MB_ICONERROR);
			return false;
		}
		//newTexture->TextureType = 2;		// 2D textures!!!!
		newTexture->TextureType = Texture_Type;
		newTexture->target = GL_TEXTURE_2D;
		newTexture->level = 0;
		newTexture->internalFormat = GL_RGBA; 
		newTexture->width = (GLsizei) newTGAFile->imageWidth;
		newTexture->height = (GLsizei) newTGAFile->imageHeight;
		newTexture->border = 0;
		//newTexture->format = (newTGAFile->bitCount / 8 == 3) ? GL_RGB : GL_RGBA; // 3 = RGB, 4 RGBA
		newTexture->format = (newTGAFile->bitCount >> 3 == 3) ? GL_RGB : GL_RGBA; // 3 = RGB, 4 RGBA
		newTexture->type = GL_UNSIGNED_BYTE;
		newTexture->texels = (GLvoid *) newTGAFile->imageData;
		newTexture->textureSrc = (void *) newTGAFile;
		newTexture->textureSrcSize = sizeof(TGAFILE);
		newTexture->textureFormatType = TGA_TEXTURE;
				
		glGenTextures(1, &newTexture->TextureName);		//Generate one unique Texture Name
		assert(!glGetError());
		new_node->SetData(newTexture, sizeof(Texture));
		TextureList->AddNodeAtEnd(*new_node);

		/*
		 *		Set Texture up
		 */
		glBindTexture(GL_TEXTURE_2D, newTexture->TextureName);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
		//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR_MIPMAP_LINEAR);	

		//gluBuild2DMipmaps(GL_TEXTURE_2D, newTexture->format, newTexture->width, newTexture->height, newTexture->format, GL_UNSIGNED_BYTE, newTexture->texels);

		glTexImage2D(GL_TEXTURE_2D, newTexture->level, newTexture->internalFormat, newTexture->width, newTexture->height, newTexture->border, newTexture->format, newTexture->type, newTexture->texels);
		assert(!glGetError());
		//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, (GLfloat) GL_DECAL);
		//assert(!glGetError());
	}
	return true;
}
void TextureManager::RemoveTGATexture(int textureName)
{
	Node *MarkerNode = NULL;
	
	TextureList->GotoHead();
	//Txture *currentTexture = 
	// Find the next Node that has the corresponding textureName
	if( ((Texture *) TextureList->GetCurrent()->GetData())->TextureName == textureName){
		free( ((TGAFILE *) ( (Texture *) TextureList->GetCurrent()->GetData())->textureSrc)->imageData); //Free the malloc image data
		delete ((Texture *) TextureList->GetCurrent()->GetData())->textureSrc;   // Deletes TGAFILE Struct
		delete (Texture *) TextureList->GetCurrent()->GetData();			//delete Texture struct
		TextureList->RemoveNodeAtBeg();
	}
	else{
		while( ((Texture *)TextureList->GetCurrent()->GetNext()->GetData())->TextureName != textureName && TextureList->GetCurrent()->GetNext() != NULL){
			TextureList->TraverseDown();
		}
		// at this point we've either found it or bust
		if(((Texture *)TextureList->GetCurrent()->GetNext()->GetData())->TextureName == textureName){
			free( ((TGAFILE *) ( (Texture *) TextureList->GetCurrent()->GetNext()->GetData())->textureSrc)->imageData); //free malloc'd memory for image data
			delete ((Texture *) TextureList->GetCurrent()->GetNext()->GetData())->textureSrc;   // Deletes TGAFILE Struct
			delete (Texture *) TextureList->GetCurrent()->GetNext()->GetData();								//delete Texture struct
			TextureList->RemoveNodeAfterNode(*TextureList->GetCurrent());
		
		}
	}
}
Texture *TextureManager::GetTGATexture(int textureName)
{
	TextureList->GotoHead();
	Texture *currentTexture = (Texture *) TextureList->GetCurrent()->GetData();

	if( currentTexture->TextureName == textureName){
		return currentTexture;
	}
	else{
		while( currentTexture->TextureName != textureName && TextureList->GetCurrent()->GetNext() != NULL){
			TextureList->TraverseDown();
			currentTexture = (Texture *) TextureList->GetCurrent()->GetData();
		}
		// at this point we've either found it or bust
		if(currentTexture->TextureName == textureName){
			return currentTexture;
		}
	}
	
	return NULL;
}
void TextureManager::DestroyTGATextureList()
{
/*	TextureList->GotoHead();
	while( TextureList->GetNumberOfNodes() > 0 ){
		free( ((TGAFILE *) ( (Texture *) TextureList->GetCurrent()->GetData())->textureSrc)->imageData ); //Free the malloc image data
		delete ((Texture *) TextureList->GetCurrent()->GetData())->textureSrc;   // Deletes TGAFILE Struct
		delete TextureList->GetCurrent()->GetData();			//delete Texture struct

		TextureList->RemoveNodeAtBeg();
	}
	// the above code is no longer needed.
*/
	TextureList->DestroyList();
}
/*
 *	DestroyTexture() needed to destroy linked list texture nodes.
 */
void DestroyTexture(void *data)
{
	Texture *texture = (Texture *) data;
	if( texture->textureFormatType == TGA_TEXTURE){
		glDeleteTextures(1, &texture->TextureName); 
		DestroyTGAImageData((TGAFILE *) texture->textureSrc);
		delete (TGAFILE *) ((Texture *) data)->textureSrc;
	}
	else if(((Texture *) data)->textureFormatType == BMP_TEXTURE){
		glDeleteTextures(1, &texture->TextureName); 
		delete (BITMAPINFOHEADER *) ((Texture *) data)->textureSrc;
	}


	delete (Texture *) data;
}

