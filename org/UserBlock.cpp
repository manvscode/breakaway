#include "userblock.h"

UserBlock::UserBlock(int x_Pos, int y_Pos)
{
	x = x_Pos;
	y = y_Pos;
	xMove = 0;
	width = Width;
	height = Height;

	collide_width = (int)(Width * 0.90);
	collide_height = (int)(Height * 0.90);

	collide_x_offset = (Width - collide_width) >> 1;
	collide_y_offset = (Height - collide_height) >> 1;
	base = 0;
	Init();

}

UserBlock::~UserBlock(void)
{
	DeInit();
}

//void UserBlock::SetMove(int x_Moved)
//{
//	//xMove = x_Moved;
//
//	x = x_Moved;
//}
void UserBlock::Draw()
{
	glPushMatrix();
		glTranslatef((GLfloat) x, (GLfloat) y, 0.0f);
		//glTranslatef((GLfloat) x, 0.0f, 0.0f);
		//glBegin(GL_QUADS);
		//	glColor3f(0.5f, 0.5f, 1.0f);
		//	//glBindTexture(gameInstance->
		//	glTexCoord2f(0.0f, 0.0f); glVertex2f(0.0f, 0.0f);
		//	glTexCoord2f(1.0f, 0.0f); glVertex2f((GLfloat) width, 0.0f);
		//	glTexCoord2f(1.0f, 1.0f); glVertex2f((GLfloat) width, (GLfloat) height);
		//	glTexCoord2f(0.0f, 1.0f); glVertex2f(0.0f, (GLfloat) height);
		//glEnd();
		glCallList(base);
	glPopMatrix();
}
void UserBlock::Init()
{
	base = glGenLists(1);
	
	glNewList(base, GL_COMPILE);
		glBegin(GL_QUADS);
			//glColor3f(0.5f, 0.5f, 1.0f);
			//glBindTexture(gameInstan
			glTexCoord2i(0, 0); glVertex2i(0, 0);
			glTexCoord2i(1, 0); glVertex2i((GLint) width, 0);
			glTexCoord2i(1, 1); glVertex2i((GLint) width, (GLint) height);
			glTexCoord2i(0, 1); glVertex2i(0, (GLint) height);
		glEnd();
	glEndList();
}
void UserBlock::DeInit()
{
	glDeleteLists(base, 1);
}
