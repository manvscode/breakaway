#pragma once

class Lines
{
public:
	float slope;
	float y_intercept;

	Lines(void);
	Lines(int x1, int y1, int x2, int y2);
	~Lines(void);
	void GetReflectedLine(Lines &Line, Lines &IncidentLine, Lines *refLine);
};
