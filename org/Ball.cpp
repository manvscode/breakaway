#include <math.h>
#include "ball.h"
#include "scene.h"

#include "lines.h"



Ball::Ball(int x_, int y_)
{
	x = x_;
	y = y_;
	width = 64 >> 2; //0.25*64 = 16
	height = 64 >> 2;

	collide_width = (int)(width * 0.90);
	collide_height = (int)(height * 0.90);

	collide_x_offset = (width - collide_width) >> 1;
	collide_y_offset = (height - collide_height) >> 1;

	SeedRandomLocation();


	//CurrentPath = new Lines(x, y, initial_x_dir, initial_y_dir);
	//BallVelocityX = 90; //relly slow
	//BallVelocityY = -350;
	BallVelocityX = 90; //relly slow
	BallVelocityY = -350;

	
	Init();
}

Ball::~Ball(void)
{
	DeInit();
}
void Ball::SetGameInstance(Game *instance)
{
	gameInstance = instance;
	ballTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_BALL_TEXTURE);
	//width = gameInstance->Texture_Manager->GetTGATexture(GAME_BALL_TEXTURE)->width;
	//height = gameInstance->Texture_Manager->GetTGATexture(GAME_BALL_TEXTURE)->height;
}
void Ball::SeedRandomLocation()
{
	//srand(GetTickCount());
	//initial_x_dir = 800 % rand();
	//srand(GetTickCount());
	//initial_y_dir = 500 % rand();
	srand(GetTickCount());
	x = (150 % rand()) + 325;
	srand(GetTickCount());
	y = (125 % rand()) + 200;

}
void Ball::MoveBall()
{
	int old_x = x;
	int old_y = y;
	static float old_t = gameInstance->GameTimer->GetTime();

	// x = spd * time
	//x = (int) BallSpeed *gameInstance->GameTimer->GetTime();  // 0.0005 is for dampining
	//y = (int) CurrentPath->slope * x + CurrentPath->y_intercept;
	//x = (int) ceilf(old_x + BallVelocityX * (gameInstance->GameTimer->GetTime() - old_t));
	//y = (int) ceilf(old_y + BallVelocityY * (gameInstance->GameTimer->GetTime() - old_t));
	x = (int) old_x + BallVelocityX * (gameInstance->GameTimer->GetTime() - old_t);
	y = (int) old_y + BallVelocityY * (gameInstance->GameTimer->GetTime() - old_t);

	old_t = gameInstance->GameTimer->GetTime();

}
//void Ball::NegateXVelocity()
//{
//	BallVelocityX = -BallVelocityX;
//}
//void Ball::NegateYVelocity()
//{
//	BallVelocityY = -BallVelocityY;
//}
void Ball::SetBallXVelocity(float v)
{
	BallVelocityX += v;
}
void Ball::SetBallYVelocity(float v)
{
	BallVelocityY += v;
}
//void Ball::NegateXYVelocity()
//{
//	BallVelocityX = -BallVelocityX;
//	BallVelocityY = -BallVelocityY;
//}
void Ball::SetPosition(int _x, int _y)
{
	x = _x;
	y = _y;
}
void Ball::Draw()
{
	
	
	glPushMatrix();
		//glRasterPos2i(x, y);
		//glColor3f(1.0f,1.0f,1.0f);
		glTranslatef( (GLfloat) x, (GLfloat) y,0.0f);
		//glPixelZoom(0.25f, 0.25f);
		//glDrawPixels(ballTexture->width, ballTexture->height, ballTexture->format, ballTexture->type, ballTexture->texels);
		glCallList(base);
	glPopMatrix();
}
void Ball::Init()
{
	base = glGenLists(1);
	glNewList(base, GL_COMPILE);
		//glRasterPos2i(x, y);
		//glPixelZoom(0.25f, 0.25f);
	//	glDrawPixels(ballTexture->width, ballTexture->height, ballTexture->format, ballTexture->type, ballTexture->texels);
		glBegin(GL_QUADS);
			//glColor4f(1.0f,1.0f,1.0f,1.0f);
			glTexCoord2i(0, 0); glVertex2i(0, 0);
			glTexCoord2i(1, 0); glVertex2i(width, 0);
			glTexCoord2i(1, 1); glVertex2i(width, height);
			glTexCoord2i(0, 1); glVertex2i(0, height);
		glEnd();
	glEndList();
}
void Ball::DeInit()
{
	glDeleteLists(base, 1);
}