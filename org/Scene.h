#pragma once

#include <math.h>
#include <windows.h>
#include "Game.h"
#include "list\dlist.h"
#include "ball.h"
#include "Block.h"
#include "UserBlock.h"
#include "ScoreManager.h"

//#include "ball.h"

/*** Textures ***/
#define GAME_SCENEBG_TEXTURE	5
#define GAME_BLOCK_TEXTURE		6
#define GAME_BALL_TEXTURE		7

#define GET_2D_DISTANCE(x1,y1,x2,y2)	(sqrt((x2-x1)^2+(y2-y1)^2))

void DestroyBlock(void *data);

class Scene
{	
	friend class Game;
	friend class Ball;
	friend class Block;
	friend class UserBlock;
	friend class ScoreManager;

public:
	Scene(Game *game_Instance);
	~Scene(void);
	bool CreateBlockList();
	void DestroyBlockList();
	void RenderScene();
	void SceneInit(void);
	void HandleUserControl();
	void SetBallPosition(int x, int y);
	inline short int Sprite_Collide(SpriteObj *object1, SpriteObj *object2);
	void TextureTest();
private:
	
	DList *BlockList;
	UserBlock	*User_Block;
	Ball		*ball;

	SpriteObj	Wall;	// used for outer wall collision

	const static int BlockWallSpace = 27;
	const static int BlockDistance = 525;
	Game *gameInstance;
	ScoreManager *Score_Manager;
public:
	void DrawClouds(float x, float y);
	int cloud_mx,
		cloud_my;
};

