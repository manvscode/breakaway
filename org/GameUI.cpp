/*	GameUI.cpp
 *
 *	Manages game's user interface.
 *	Coded by Joseph A. Marrero
 */
#include "gameui.h"


GameUI::GameUI(Game *game_Instance)
{
	gameInstance = game_Instance;
	Selection = 0;
}

GameUI::~GameUI(void)
{
}
void GameUI::DisplayMenuInterface()
{
	static unsigned int yMover = 129;
	
	Texture *currentTexture = NULL;

	//glRasterPos2i(0, 599);
	glRasterPos2i(0, 0);
	currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_OPENING_BG);
	glPixelZoom((GLfloat) 800.0 / currentTexture->width,
				(GLfloat) 600.0 / currentTexture->height);

	glDrawPixels( currentTexture->width,
				  currentTexture->height, 
				  currentTexture->format,
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) currentTexture->texels );

	glRasterPos2i(571, 193);
	glPixelZoom(1.0f,1.0f);
	currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_MENU_BUTTON01);
	glDrawPixels( currentTexture->width,
				  currentTexture->height, 
				  currentTexture->format,
				  GL_UNSIGNED_BYTE, 
				  (GLubyte *) currentTexture->texels );
	glRasterPos2i(571, 129);
	currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_MENU_BUTTON02);
	glDrawPixels( currentTexture->width,
				  currentTexture->height, 
				  currentTexture->format,
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) currentTexture->texels );
	
	if(gameInstance->keys[VK_UP] == TRUE){
		//yMover = 129;
		Selection = 1;
	}
	else if(gameInstance->keys[VK_DOWN] == TRUE){
		//yMover = 193;
		Selection = 2;
	}

	if( Selection == 1){
		//for( int yMover = 129; yMover <= 193; yMover += 2){
			
			glRasterPos2i(520, yMover);
			currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_MENU_SELECTOR);
			glDrawPixels( currentTexture->width,
						  currentTexture->height, 
						  currentTexture->format,
						  GL_UNSIGNED_BYTE,
						  (GLubyte *) currentTexture->texels );
		//	gameInstance->GameTimer->Wait(0.1);
		//}
			if( yMover <= 193  )
				yMover += 8;
	}
	else if(Selection == 2){
		//for( int yMover = 193; yMover >= 129; yMover -= 2){
			glRasterPos2i(520, yMover);
			currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_MENU_SELECTOR);
			glDrawPixels( currentTexture->width,
						  currentTexture->height, 
						  currentTexture->format,
						  GL_UNSIGNED_BYTE,
						  (GLubyte *) currentTexture->texels );
		//	}
			if( yMover >= 129 ) yMover -= 8;
	}

	if( gameInstance->keys[VK_RETURN] == TRUE){
		switch(Selection){
			case 1:
				gameInstance->GameScene->SceneInit();
				gameInstance->GameScene->DestroyBlockList();
				gameInstance->GameScene->CreateBlockList();
				gameInstance->GameScene->SetBallPosition(392, 400);
				gameInstance->SetInGameState();
				gameInstance->GameTimer->ResetTimer();
				break;
			case 2:
				PostQuitMessage(0);
				break;
			default:
				break;
		}
	}


}