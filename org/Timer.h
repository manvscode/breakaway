#pragma once
/*		Timer.h
 *
 *		Timer object to keep time from a initial moment.
 *		Coded by Joseph A. Marrero
 */
class Timer
{
private:
	float		timeFromStart;
	UINT64		ticksPerSecond;

	void InitTimer();
public:
	Timer(void);
	~Timer(void);

	void ResetTimer();
	bool Wait(float time_secs);
	float GetTime();
};
