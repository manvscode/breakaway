
#include <stdio.h>
#include <gl/glaux.h>
#include "scene.h"
#include ".\scene.h"



#define ENERGY_LOSS		25



Scene::Scene(Game *game_Instance)
{
	
	gameInstance = game_Instance;
	BlockList = new DList( DestroyBlock );
	Score_Manager = game_Instance->Score_Manager;
	
	User_Block = new UserBlock(392, 50);
	ball = new Ball(392, 400);

	//SceneInit();
	//CreateBlockList();
	
	Wall.width = 740;
	Wall.height = 515;
	Wall.x = 22;
	Wall.y = 20;

	cloud_mx = 0;
	cloud_my = 0;

}
Scene::~Scene(void)
{
	delete BlockList;
	delete User_Block;
	delete ball;
}
bool Scene::CreateBlockList()
{
	register int yDistance = BlockDistance;

	for(register int BlockRow = 0; BlockRow < 6; BlockRow++){ // 6
		for(register int BlockCol = 0; BlockCol < 8; BlockCol++){ // 8
			//Block *newBlock = new Block(BlockWallSpace + BlockCol * 93, yDistance); // 93 = BlockWidth
			Block *newBlock = new Block(BlockWallSpace + ((BlockCol  << 6) + (BlockCol << 4) + (BlockCol << 3) + (BlockCol << 2)  + 1), yDistance); // Equivalent to above commented line but faster in theory.
			Node *newNode = new Node;
			
			if(newBlock == NULL || newNode == NULL)
				return false;
			
			newNode->SetData(newBlock, sizeof(Block));
			BlockList->AddNodeAtBeg(*newNode);
		}
		yDistance -= 18; // 18 = BlockHeight
	}
	return true;
}
void Scene::DestroyBlockList()
{
	BlockList->DestroyList();
}
void Scene::RenderScene()
{
	Texture *currentTexture = NULL;
	Node *prevNode = NULL;
	//glClearColor(0.0f, 0.0f, 0.0f, 0.5f);

	
#ifdef _DEBUG
	//char buffer[32];
	//sprintf(buffer, "V_x = %d, V_y = %d", ball->BallVelocityX, ball->BallVelocityY);
	//gameInstance->Text_Out->PrintString(600,50, buffer);
#endif

	currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_SCENEBG_TEXTURE);
	
	// Draw Background
	glRasterPos2i(0, 0);
	glPixelZoom((GLfloat) 800.0 / currentTexture->width,
				(GLfloat) 600.0 / currentTexture->height);

	glDrawPixels( currentTexture->width,
				  currentTexture->height, 
				  currentTexture->format,
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) currentTexture->texels );

	
	BlockList->GotoHead();


	//glFlush();
	
	//Check for wall collision
	if(ball->x > Wall.x + Wall.width){
		ball->x = Wall.x + Wall.width;
		ball->NegateXVelocity();
		ball->BallVelocityX -= ENERGY_LOSS;
	}
	else if(ball->x < Wall.x){
		ball->x = Wall.x;
		ball->NegateXVelocity();
		ball->BallVelocityX -= ENERGY_LOSS;
	}
	else if( ball->y > Wall.y + Wall.height ){
		ball->y = Wall.y + Wall.height;
		ball->NegateYVelocity();
		ball->BallVelocityY -= ENERGY_LOSS;
	}
	
	else if(ball->y < Wall.y && gameInstance->GameState != 2){
		//ball->y = 25;
		
		ball->y = Wall.y;
		//ball->NegateYVelocity();
		//ball->BallVelocityY -= ENERGY_LOSS;
		gameInstance->SetGameOverState();

		//ball->y = -10;
		return;
	}
		
	//Check for Collison with Ball and UserBlock
	if(Sprite_Collide(ball, User_Block) == TRUE ) {
		// from  the left
		if(ball->x < User_Block->x){
			ball->x = User_Block->x;
			ball->NegateXVelocity();
			ball->BallVelocityX -= ENERGY_LOSS;
		}
		// from the right
		else if(ball->x > User_Block->x + User_Block->width){
			ball->x = User_Block->x + User_Block->width;
			ball->NegateXVelocity();
			ball->BallVelocityX -= ENERGY_LOSS;
		}
		// from the top
		else if( ball->y < User_Block->y + User_Block->height && ball->y > User_Block->y ){
			ball->y = User_Block->y + User_Block->height;
			ball->NegateYVelocity();
			ball->BallVelocityY -= ENERGY_LOSS;
		}
		// from the bottom
		//else if(ball->y + ball->height < User_Block->y){
		//	ball->y = User_Block->y -  ball->height;
		//	ball->NegateYVelocity();
		//}
		else {
			ball->y = User_Block->y -  ball->height;
			ball->NegateYVelocity();
			ball->BallVelocityY -= ENERGY_LOSS;
		}

	}


	glEnable(GL_TEXTURE_2D);

	// Draw Ball
	glBindTexture(GL_TEXTURE_2D, GAME_BALL_TEXTURE);
	ball->Draw();
	ball->MoveBall();

	

	
	//glBindTexture(GL_TEXTURE_2D, GAME_BLOCK_TEXTURE);
	//currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_BLOCK_TEXTURE);
	glBindTexture(GL_TEXTURE_2D, GAME_BLOCK_TEXTURE);

	// Draw User Block
	HandleUserControl();
	User_Block->Draw();




	//currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_BLOCK_TEXTURE);
//	glBindTexture(GL_TEXTURE_2D, currentTexture->TextureName);
	glBindTexture(GL_TEXTURE_2D, GAME_BLOCK_TEXTURE);
	
	if(BlockList->GetNumberOfNodes() == 0 && gameInstance->GameState != 4){
		gameInstance->SetGameNextLevelState();
		ball->SetBallXVelocity(25);
		ball->SetBallXVelocity(25);
		return;
		//gameInstance->Text_Out->PrintString(400,300, "Next Level!");
	}

	//Draw Blocks in list
	for(register unsigned int i = 0; i < BlockList->GetNumberOfNodes(); i++){
		
		Block *currentBlock = (Block *) BlockList->GetCurrent()->GetData();
		//Check for Collison with Ball and 1st Block
		if(Sprite_Collide(ball, currentBlock) == TRUE){
			// from  the left
			if(ball->x < currentBlock->x ){
				ball->x = currentBlock->x;
				ball->NegateXVelocity();
				ball->BallVelocityX -= ENERGY_LOSS;
			}
			// from the right
			else if(ball->x > currentBlock->x + currentBlock->width){
				ball->x = currentBlock->x + currentBlock->width;
				ball->NegateXVelocity();
				ball->BallVelocityX -= ENERGY_LOSS;
			}
			// from the top
			else if( ball->y < currentBlock->y + currentBlock->height && ball->y > currentBlock->y ){
				ball->y = currentBlock->y + currentBlock->height;
				ball->NegateYVelocity();
				ball->BallVelocityY -= ENERGY_LOSS;
			}
			// from the bottom
			else {
				ball->y = currentBlock->y -  ball->height;
				ball->NegateYVelocity();
				ball->BallVelocityY -= ENERGY_LOSS;
			}
			
			if(prevNode != NULL){
				BlockList->RemoveNodeAfterNode(*prevNode);
				Score_Manager->AdjustScore();
				return; // done for this frame!
			}
			else {
				BlockList->RemoveNodeAtBeg();
				Score_Manager->AdjustScore();
				return;	// done for this frame!
			}
			//BlockList->GotoHead();
			//break;
		}
		prevNode = BlockList->GetCurrent();
		currentBlock->Draw();
		BlockList->TraverseDown();
	}

	if( cloud_mx <= 800){
		glBindTexture(GL_TEXTURE_2D, 8);
		glColor4f(0.0f,0.0f,0.0f, 0.25f);
		if(cloud_mx > 0) DrawClouds(cloud_mx-800 ,0);
		DrawClouds(cloud_mx, 0);
		cloud_mx += 1;
	}
	else{
		cloud_mx = 0;
	}

	
	if( cloud_my <= 600){
		glBindTexture(GL_TEXTURE_2D, 8);
		//glRotatef(
		glColor4f(0.0f,0.0f,0.0f, 0.08f);
		if(cloud_my > 0) DrawClouds(0 ,cloud_my-600);
		DrawClouds(0, cloud_my);
		cloud_my += 1;
	}
	else{
		cloud_my = 0;
	}
	
	glDisable(GL_TEXTURE_2D);
}
void Scene::SceneInit(void)
{

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	//glClearDepth(1.0f);
	//glEnable(GL_DEPTH_TEST);	
	//glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, (GLfloat) GL_DECAL);

	//User_Block = new UserBlock(392, 150);
	//ball = new Ball(392, 400);
	
	ball->SetGameInstance(gameInstance);
	ball->SeedRandomLocation();

}
void Scene::HandleUserControl()
{
	static int x_Position = User_Block->x;

	if( gameInstance->keys[VK_LEFT] == TRUE){
		x_Position -= 20;//User_Block
		
		if(x_Position < 30)
			x_Position = 30;
	}
	else if( gameInstance->keys[VK_RIGHT] == TRUE){
		x_Position += 20; //Do somethin
		
		if(x_Position > 655)
			x_Position = 655;
	}

	User_Block->SetMove(x_Position);
}
void Scene::SetBallPosition(int x, int y)
{
	ball->SetPosition(x, y);
}
short int Scene::Sprite_Collide(SpriteObj *object1, SpriteObj *object2)
{
    int left1, left2;
    int right1, right2;
    int top1, top2;
    int bottom1, bottom2;

	//if(object1 == NULL || object2 == NULL)	// its lil' faster to be unsafe
	//	return 0;

	left1 = object1->x + object1->collide_x_offset;
	left2 = object2->x + object2->collide_x_offset;
	right1 = left1 + object1->collide_width;
	right2 = left2 + object2->collide_width;
	top1 = object1->y + object1->collide_y_offset;
	top2 = object2->y + object1->collide_y_offset;
	bottom1 = top1 + object1->collide_height;
	bottom2 = top2 + object2->collide_height;

    if (bottom1 < top2) return 0;
    if (top1 > bottom2) return 0;
  
    if (right1 < left2) return 0;
    if (left1 > right2) return 0;

    return	1;


}

void DestroyBlock(void *data)
{
	delete (Block *) data;
}
void Scene::TextureTest()
{
}
void Scene::DrawClouds(float x, float y)
{
	
	glPushMatrix();
		glTranslatef(x, y, 0.0f);
		//glColor4f(0.0f,0.0f,0.0f, 0.25f);
		glBegin(GL_QUADS);
				glTexCoord2i(0, 0); glVertex2i(0, 0);
				glTexCoord2i(1, 0); glVertex2i(800, 0);
				glTexCoord2i(1, 1); glVertex2i(800, 600);
				glTexCoord2i(0, 1); glVertex2i(0, 600);
		glEnd();
	glPopMatrix();

}
