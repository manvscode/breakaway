#pragma once
#include <windows.h>
#include <gl/gl.h>
#include "SpriteObj.h"

class Block : public SpriteObj
{
	friend class Scene;
private:
	//const static int width = 93;
	//const static int height = 15;
	//int x, y;
	unsigned int base;
	
public:
	Block(int x_pos, int y_pos);
	~Block(void);
	void DrawBlackOutline();
	void Draw();
	void Init();
	void DeInit();
	
};
