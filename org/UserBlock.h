#pragma once
#include "game.h"
#include "SpriteObj.h"


class UserBlock : public SpriteObj
{
	friend class Scene;
	//int x, y;
	int xMove;
	static const int Width = 112,
					 Height = 20;
	unsigned int base;	// for vertex list

public:
	UserBlock(int x_Pos, int y_Pos);
	~UserBlock(void);
	void Init();
	void DeInit();
	inline void SetMove(int x_Moved)
	{
		x = x_Moved;
	}
	void Draw();

};
