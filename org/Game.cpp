/*	Game.cpp
 *
 *	Manages game's states and other managers.
 *	Coded by Joseph A. Marrero
 */
#include <stdio.h>
#include "game.h"


Game::Game(void)
{
	hDC = NULL;
	GameState = GAME_START;			//Display UI
	lastUpdate = 0;
	fpsUpdateInterval = 0.5f;		// 0.5
	numFrames = 0;
	fps = 0;

	GameUserInterface = NULL;
	GameScene = NULL;
	Texture_Manager = NULL;
	GameTimer = NULL;
	Text_Out = NULL;
	Score_Manager = NULL;
}

Game::~Game(void)
{
	DeInitGame();
	//delete instance;
	
}
void Game::SetHDC(HDC g_hDC)
{
	hDC = g_hDC;
}
bool Game::InitGame()
{
	// OpenGL initialization...

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	

	glEnable(GL_ALPHA_TEST);
	
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

	//glEnable(GL_TEXTURE_2D);  //Not here
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_COLOR);

	glShadeModel(GL_SMOOTH);
	//glEnable(GL_SRC_ALPHA);
	//glEnable(GL_COLOR_MATERIAL);
	glClearDepth(1.0f);							// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	
	//glDepthMask(GL_TRUE);
	glPixelStorei( GL_PACK_ALIGNMENT, 4);
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4);

	//glEnable(GL_TEXTURE_2D);

	if( (GameTimer = new Timer) == NULL ){
		MessageBox(NULL, "Unable to create a timer!", "Error", MB_OK | MB_ICONERROR);	
		return false;	
	}
	if( (GameUserInterface = new GameUI(this)) == NULL){
		MessageBox(NULL, "Unable to create GameUI!", "Error", MB_OK | MB_ICONERROR);	
		return false;
	}
	if( (Text_Out = new TextRasterizer(hDC)) == NULL){
		MessageBox(NULL, "Unable to create TextRasterizer!", "Error", MB_OK | MB_ICONERROR);
		return false;
	}
	else{
		Text_Out->CreateBitmapFont("Terminator Two", 28);
	}
#ifdef __SHOW_FPS__
//	MessageBox(NULL,"__SHOW_FPS__ defined","__SHOW_FPS__", MB_OK);
	if( (FPS_Text = new TextRasterizer(hDC) ) == NULL){
		MessageBox(NULL, "Unable to create TextRasterizer for FPS!", "Error", MB_OK | MB_ICONERROR);
		return false;
	}
	else{
		FPS_Text->CreateBitmapFont("Tahoma", 12);
	}
#endif

	if( (Score_Manager = new ScoreManager(this)) == NULL){
		MessageBox(NULL, "Unable to create ScoreManager!", "Error", MB_OK | MB_ICONERROR);
		return false;
	}
	if( (Texture_Manager = new TextureManager) == NULL){
		MessageBox(NULL, "Unable to create TextureManager!", "Error", MB_OK | MB_ICONERROR);
		return false;
	}
	else {
		Texture_Manager->AddTGATexture("Textures\\title.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\button01.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\button02.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\selector.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\scenebg.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\block.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\ball.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\clouds.tga", 2);
	}
	if((GameScene = new Scene(this)) == NULL){
		MessageBox(NULL, "Unable to create GameScene!", "Error", MB_OK | MB_ICONERROR);
		return false;
	}
	Text_Out->ClearFont();
	Text_Out->CreateBitmapFont("Arial Black", 28);

	return true;
}
void Game::DeInitGame()
{
	delete GameTimer;
	delete GameUserInterface;
	delete GameScene;
	delete Texture_Manager;
	delete Score_Manager;
	delete Text_Out;
//#ifdef __SHOW_FPS__
	delete FPS_Text;
//#endif
	
}
void Game::SetGameStartState()
{
	GameState = GAME_START;
}
void Game::SetInGameState()
{
	GameState = GAME_INGAME;
}
void Game::SetGameOverState()
{
	GameState = GAME_GAMEOVER;
}
void Game::SetGameNextLevelState()
{
	GameState = GAME_NEXTLEVEL;
}
void Game::Render()
{
	glDisable(GL_TEXTURE_2D);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glClear(GL_ALL_ATTRIB_BITS);
	//glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
	//glLoadIdentity();
	

	switch(GameState){
		case GAME_START:
			
			GameUserInterface->DisplayMenuInterface();
			break;
		case GAME_INGAME:

			GameScene->RenderScene();
			
#ifdef __SHOW_FPS__
			glColor3f(1.0f,1.0f,0.3f);
			UpdateFPS();
			FPS_Text->PrintString(0,3, fps_string);
#endif
			glColor3f(1.0f,1.0f,1.0f);
			Score_Manager->WriteScore();
			
			break;
		case GAME_GAMEOVER:
			
			//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_COLOR);
			Score_Manager->ResetScore();
			Text_Out->PrintString(350, 300, "Game Over!");
			SwapBuffers(hDC);
			GameTimer->Wait(2);

			GameScene->SceneInit();
			GameScene->DestroyBlockList();
			//GameScene->CreateBlockList();
			GameScene->SetBallPosition(392, 400);
			//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			SetGameStartState();
			break;
		case GAME_WIN:
			break;
		case GAME_NEXTLEVEL:
			
			//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_COLOR);
			GameTimer->ResetTimer();
			Text_Out->PrintString(350,300,"Next Level");
			SwapBuffers(hDC);
			GameTimer->Wait(2);
			//GameScene->SceneInit();
			GameScene->DestroyBlockList();	// potentially not needed!
			GameScene->CreateBlockList();
			GameScene->SetBallPosition(392, 400);
			SetInGameState();
			//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			
			GameTimer->ResetTimer();
		
			return;
			break;
		default:
			break;
	}

	

	SwapBuffers(hDC);
	//glFlush();

}
Game* Game::instance = NULL;
Game* Game::GetInstance()
{
	if(instance == NULL){
		instance = new Game;
		return instance;
	}
	else
		return instance;
}
void Game::UpdateFPS()
{
	int dec = 0,
		sign = 0;
	numFrames++;

	float currentUpdate = GameTimer->GetTime();

	if( currentUpdate - lastUpdate >= fpsUpdateInterval )
	{
		fps = numFrames / (currentUpdate - lastUpdate);
		lastUpdate = currentUpdate;
		numFrames = 0;
	}

	sprintf(fps_string, "%0.1f fps", fps);
}
