#pragma once
#include "game.h"
#include "lines.h"
#include "SpriteObj.h"
#include "TextureManager.h"

#define MOTION_DAMPENING	1

class Ball : public SpriteObj
{
	friend class Game;
	friend class Scene;
	friend class TextureManager;
	friend class Lines;
private:
	int radius;
	//int x, y;  // Ball's texture Lower Left Corner
	Game *gameInstance;
	
	int initial_x_dir, initial_y_dir; // Random (x,y) to create a random dir
	//Lines *CurrentPath;
	//Lines *NextPath;
	float BallVelocityX;
	float BallVelocityY;
	unsigned int base;
	struct tagTexture *ballTexture;
public:
	Ball(int x_, int y_);
	~Ball(void);
	void SetGameInstance(Game *instance);
	void SeedRandomLocation();
	void MoveBall();
	void SetBallXVelocity(float v);
	void SetBallYVelocity(float v);
	inline void NegateXVelocity()
	{
		BallVelocityX = -BallVelocityX;
	}
	inline void NegateYVelocity()
	{
		BallVelocityY = -BallVelocityY;
	}
	inline void NegateXYVelocity()
	{	
		BallVelocityX = -BallVelocityX;
		BallVelocityY = -BallVelocityY;
	}
	void SetPosition(int _x, int _y);
	void Draw();
	void Init();
	void DeInit();
};
