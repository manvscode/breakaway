#pragma once
/*	Game.h
 *
 *	Manages game's states and other managers.
 *	Coded by Joseph A. Marrero
 */
#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "GameUI.h"
#include "Scene.h"
#include "TextureManager.h"
#include "Timer.h"
#include "ScoreManager.h"
#include "TextRasterizer.h"
//#include <stdio.h>
#include <stdlib.h>


class Game
{
	friend class GameUI;
	friend class Scene;
	friend class TextureManager;
	friend class ScoreManager;
	friend class TextRasterizer;
	friend class Ball;
private:
	static Game *instance;
	GameUI *GameUserInterface;
	Scene *GameScene;
	TextureManager *Texture_Manager;
	Timer *GameTimer;
	TextRasterizer *Text_Out;
	ScoreManager *Score_Manager;
#ifdef __SHOW_FPS__
	TextRasterizer *FPS_Text;
#endif


	// FPS stuff
	float lastUpdate;
	float fpsUpdateInterval;
	unsigned int  numFrames;
	float fps;
	char fps_string[9];

	HDC hDC;
	enum tagGameState {
		GAME_START = 0, 
		GAME_INGAME = 1,
		GAME_GAMEOVER = 2,
		GAME_WIN = 3,
		GAME_NEXTLEVEL = 4
	}GameState;

protected:

public:
	Game(void);
	~Game(void);
	static Game *GetInstance();
	void SetHDC(HDC g_hDC);
	bool InitGame();
	void DeInitGame();
	void SetGameStartState();
	void SetGameOverState();
	void SetInGameState();
	void SetGameNextLevelState();


	void Render();
	void UpdateFPS();
	bool keys[256];	
};
