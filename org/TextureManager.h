#pragma once
/*	TextureManager.h
 *
 *	Manages game textures.
 *	Coded by Joseph A. Marrero
 */
//#include <gl/gl.h>
#include "Game.h"
#include "list\dlist.h"

#define	BMP_TEXTURE			0
#define	TGA_TEXTURE			1

void DestroyTexture(void *data);



typedef struct tagTexture {
	int			TextureType;		//1D = 1, 2D = 2, 3D =3;
	GLenum		target;				// GL_TEXTURE_1D, GL_TEXTURE_2D, OR GL_TEXTURE_3D
	GLint		level;				// Resolution of Texture Map
	GLint		internalFormat;		// GL_LUMINANCE, GL_LUMINANCE_ALPHA, GL_RGB, GL_RGBA
	GLsizei		width;				// Texture width
	GLsizei		height;				// Texture height
	GLint		border;				// no border = 0, or 1
	GLenum		format;				// GL_RGB, or GL_RGBA, GL_COLOR_INDEX
	GLenum		type;				// GL_BYTE, GL_UNSIGNED_BYTE, GL_SHORT, GL_UNSIGNED_SHORT, GL_INT, GL_UNSIGNED_INT, GL_FLOAT
									// GL_BITMAP
	GLvoid		*texels;			// ptr to imageData

	void		*textureSrc;		// ptr to Image Structure like TGAFILE
	unsigned int textureSrcSize;	// sizeof of ImageStructure
	unsigned int textureFormatType;		// BMP_TEXTURE, TGA_TEXTURE
	GLuint		TextureName;		// a unique unsigned int!
}Texture;

class TextureManager
{
	friend class Game;
private:
	DList *TextureList;



public:
	TextureManager(void);
	~TextureManager(void);
	bool AddTexture(char *filename, unsigned int Texture_Type);
	bool AddTGATexture(char *filename, unsigned int Texture_Type);		// Add a Texture;
	void RemoveTGATexture(int textureName);							// Remove a Texture by its Texture name;
	Texture *GetTGATexture(int textureName);
	void DestroyTGATextureList();
	bool AddTGATexture2(void);
};


