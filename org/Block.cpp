

#include "block.h"

Block::Block(int x_pos, int y_pos)
{
	x = x_pos;
	y = y_pos;
	width = 93;
	height = 18;

	collide_width = (int)(width * 0.9);
	collide_height = (int)(height * 0.9);

	collide_x_offset = (width - collide_width) >> 1;
	collide_y_offset = (height - collide_height) >> 1;
	base = 0;
	Init();
}

Block::~Block(void)
{
	DeInit();
}
void Block::DrawBlackOutline()
{
	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glLineWidth(2);

		glVertex2i(0, 0);
		glVertex2i((GLint) width, 0);

		glVertex2i(0, 0);
		glVertex2i(0, (GLint) height);

		glVertex2i((GLint) width, 0);
		glVertex2i((GLint) width, (GLint) height);

		glVertex2i((GLint) width, (GLint) height);
		glVertex2i(0, (GLint) height);
	glEnd();
}
//Renders relative to bottom life
void Block::Draw()
{
	glPushMatrix();
		glTranslatef((GLfloat) x , (GLfloat) y, 0.0f);
		/*
		DrawBlackOutline();
		glBegin(GL_QUADS);
			glColor3f(0.5f, 0.5f, 0.5f);
			//glBindTexture(gameInstance->
			glTexCoord2f(0.0f, 0.0f); glVertex2f(0.0f, 0.0f);
			glTexCoord2f(1.0f, 0.0f); glVertex2f((GLfloat) width, 0.0f);
			glTexCoord2f(1.0f, 1.0f); glVertex2f((GLfloat) width, (GLfloat) height);
			glTexCoord2f(0.0f, 1.0f); glVertex2f(0.0f, (GLfloat) height);
		glEnd();
		*/
		glCallList(base);
	glPopMatrix();

}
void Block::Init()
{
	base = glGenLists(1);
	
	glNewList(base, GL_COMPILE);
		glBegin(GL_LINES);
			//glColor3f(0.0f,0.0f,0.0f);
			glLineWidth(2.0f);

			glVertex2i(0, 0);
			glVertex2i((GLint) width, 0);

			glVertex2i(0, 0);
			glVertex2i(0, (GLint) height);

			glVertex2i((GLint) width, 0);
			glVertex2i((GLint) width, (GLint) height);

			glVertex2i((GLint) width, (GLint) height);
			glVertex2i(0, (GLint) height);
		glEnd();

		glBegin(GL_QUADS);
			//glColor3f(0.8f, 0.8f, 0.8f);
			//glBindTexture(gameInstance->
			glTexCoord2i(0, 0); glVertex2i(0, 0);
			glTexCoord2i(1, 0); glVertex2i((GLint) width, 0);
			glTexCoord2i(1, 1); glVertex2i((GLint) width, (GLint) height);
			glTexCoord2i(0, 1); glVertex2i(0, (GLint) height);
		glEnd();
	glEndList();
}
void Block::DeInit()
{
	glDeleteLists(base, 1);
}