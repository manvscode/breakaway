#include ".\ogldevinterface.h"

OGLDevInterface::OGLDevInterface(void)
{
	hInstance = GetModuleHandle(NULL);
	hDC = NULL;
	hRC = NULL;
	fullscreen = false;
}

OGLDevInterface::~OGLDevInterface(void)
{
}

GLvoid OGLDevInterface::ReSizeGLScene(GLsizei width, GLsizei height)
{
	if(height== 0)
		height = 1;

	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective( 45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
int OGLDevInterface::InitGL(GLvoid)
{
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	return TRUE;
}
int OGLDevInterface::DrawGLScene(GLvoid)
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	return TRUE;
}
bool OGLDevInterface::CreateGLWindow(char *title, int width, int height, int bits, bool fullscreenflag)
{
	WNDCLASSEX	wndClass;
	//HWND hWnd;
	RECT WindowRect;
	MSG msg;
	WindowRect.left = (long) 0;
	WindowRect.right = (long) width;
	WindowRect.top = (long) 0;
	WindowRect.bottom = (long) height;
	bool done = false;

	fullscreen = fullscreenflag;
	BitsPerPixel = bits;

	wndClass.cbSize = sizeof( WNDCLASSEX );
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc =  WndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = GetModuleHandle(NULL);
	//wndClass.hIcon = LoadIcon(NULL, IDI_ICON);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = "OpenGL";
	//wndClass.hIconSm = LoadIcon(hInstance, IDI_ICON);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if( !RegisterClassEx(&wndClass) )
		return false;

	


	hWnd = CreateWindowEx(NULL, "OpenGL", title, WS_OVERLAPPEDWINDOW |
		WS_VISIBLE | WS_SYSMENU, 100, 100, width, height, NULL, NULL, hInstance, NULL);
	
	if(!hWnd){
		KillGLWindow();
		MessageBox(NULL,"Window creation error.","Error", MB_OK | MB_ICONERROR);
		return false;
	}

	while(!done){
		PeekMessage(&msg, hWnd, NULL, NULL, PM_REMOVE);

		if(msg.message == WM_QUIT)
			done = true;
		else {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

}
GLvoid OGLDevInterface::KillGLWindow(GLvoid)
{
	if(fullscreen) {
		ChangeDisplaySettings(NULL, 0);
		ShowCursor(TRUE);
	}
	if(hRC){
		if( !wglMakeCurrent(NULL, NULL) ){
			MessageBox(NULL,"Release of DC and RC failed.", "Shutdown Error", MB_OK | MB_ICONERROR);
		}

		if( !wglDeleteContext(hRC) ){
			MessageBox(NULL, "Release of Rendering Context failed!", "Shutdown Error", MB_OK | MB_ICONERROR);
		}
		hRC = NULL;
	}
	if(hDC && !ReleaseDC(hWnd, hDC)){
		MessageBox(NULL, "Release of Device Context failed.", "Shutdown Error", MB_OK | MB_ICONERROR);
		hDC = NULL;
	}
	if(hWnd && !DestroyWindow(hWnd)){
		MessageBox(NULL, "Could not release hWnd.", "Shutdown Error", MB_OK | MB_ICONERROR);
		hWnd=NULL;
	}
	if(!UnregisterClass("OpenGL", hInstance)){
		MessageBox(NULL, "Could not unregister class.", "Shutdown Error", MB_OK | MB_ICONERROR);
		hInstance = NULL;
	}

}
LRESULT CALLBACK OGLDevInterface::WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message){
		case WM_CREATE:
			return 0;
			break;
		case WM_CLOSE:
			PostQuitMessage(0);
			return 0;
			break;
		case WM_SIZE:
			return 0;
			break;
		default:
			break;
	}
	return DefWindowProc(hwnd,message, wParam,lParam);
}
GLvoid OGLDevInterface::GoFullscreen(GLvoid)
{
	if(fullscreen){
		DEVMODE dmScreenSettings;
		memset( &dmScreenSettings, 0, sizeof(dmScreenSettings) );
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = width;
		dmScreenSettings.dmPelsHeight = height;
		dmScreenSettings.dmBitsPerPel = BitsPerPixel;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		if( ChangeDisplaySettings( &dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL ){
			if(MessageBox(NULL, "The Requested Fullscreen mode is not supported by\nyour video card. Use windowed mode instead?", "Fullscreen?", MB_YESNO | MB_ICONEXCLAMATION) == IDYES) {
				fullscreen = false;
			}
			else {
				MessageBox(NULL, "Program will now close.", "Error", MB_OK | MB_ICONSTOP);
				KillGLWindow();
			}
		}
	
	}
}
GLvoid OGLDevInterface::ToggleFullscreen(GLvoid)
{
	fullscreen = !fullscreen;
}