#include <stdio.h>
#include "scoremanager.h"

ScoreManager::ScoreManager(Game *game_Instance)
{
	GameInstance = game_Instance;
	out = game_Instance->Text_Out;
	score = 0;
}

ScoreManager::~ScoreManager(void)
{
}
void ScoreManager::AdjustScore(int scoreAdj)
{
	score += scoreAdj;
}
void ScoreManager::AdjustScore()
{
	score += 5;
}
void ScoreManager::WriteScore()
{
	
	sprintf(ScoreString, "Score: %d", score);
	
	//shadow
	glColor4f(0.1f,0.1f,0.1f,0.5f);
	out->PrintString(13, 565, ScoreString);
	
	glColor4f(1.0f,1.0f,1.0f, 1.0f);
	out->PrintString(8, 570, ScoreString);
	

}
void ScoreManager::ResetScore()
{
	score = 0;
}