#include <math.h>
#include "ball.h"
#include "scene.h"

#include "lines.h"



Ball::Ball(int x_, int y_)
{
	x = x_;
	y = y_;
	width = 64 >> 2; //0.25*64 = 16
	height = 64 >> 2;

	collide_width = (int)(width * 0.90);
	collide_height = (int)(height * 0.90);

	collide_x_offset = (width - collide_width) >> 1;
	collide_y_offset = (height - collide_height) >> 1;

	SeedRandomLocation();


	BallVelocityX = 200; //relly slow
	BallVelocityY = -250;

	
	Init();
}

Ball::~Ball()
{
	DeInit();
}
void Ball::SetGameInstance(Game *instance)
{
	gameInstance = instance;
	ballTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_BALL_TEXTURE);
}
void Ball::SeedRandomLocation()
{
	srand(GetTickCount());
	y = (rand() % 125) + 267;
	x = (rand() % 250) + 325;
	
}
void Ball::MoveBall()
{
	x = (int) x + BallVelocityX  * gameInstance->getTimeStep();
	y = (int) y + BallVelocityY  * gameInstance->getTimeStep();
}

void Ball::SetBallXVelocity(float v)
{
	BallVelocityX += v;
}
void Ball::SetBallYVelocity(float v)
{
	BallVelocityY += v;
}

void Ball::AdjustYVelocity(float v)
{
	BallVelocityY += v;
}
void Ball::SetPosition(int _x, int _y)
{
	x = _x;
	y = _y;
}
void Ball::Draw()
{
	glPushMatrix();
		glTranslatef( (GLfloat) x, (GLfloat) y,0.0f);
		glCallList(base);
	glPopMatrix();
}
void Ball::Init()
{
	base = glGenLists(1);
	glNewList(base, GL_COMPILE);
		glBegin(GL_QUADS);
			glTexCoord2i(0, 0); glVertex2i(0, 0);
			glTexCoord2i(1, 0); glVertex2i(width, 0);
			glTexCoord2i(1, 1); glVertex2i(width, height);
			glTexCoord2i(0, 1); glVertex2i(0, height);
		glEnd();
	glEndList();
}
void Ball::DeInit()
{
	glDeleteLists(base, 1);
}