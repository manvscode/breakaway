#pragma once
/*		list.h
 *
 *		Singly linked list data structure.
 *		Coded by Joseph A. Marrero
 *		4/1/04, modified 10/28/04
 *
 *		Current Limitations: SaveToFile, OpenFromFile() has use a constant DataSize.
 *		I suggest using an Object to encapsulate your data.
 */
#include "Node.h"

#ifdef __DEBUG_LIST__
	#ifndef __DEBUG_NODE__
	#define __DEBUG_NODE__
	#endif
#endif
typedef class List
{
	friend class Node;
	friend class DList;
	friend class CircularList;
	friend class CircularDList;
protected:
	void (*DestroyNodeData)(void *Data);									// Pointer to Destroy node data. MUST BE SET or leaks will occur.
	bool (*CompareNode)(Node *node1, Node *node2);							// Used to compare two nodes, Must be set with SetCompare()
	
public:
	List(void);
	List( void (*DestroyNodeData)(void *Data) );
	virtual ~List(void);
	
	/* Creation / deletion */
	Node *CreateNode();														// Creates a node, and returns its instance.
	Node *CreateNode(void *Data, int DataSize);								// Creates a node, and returns its instance.
	bool DestroyNode(Node *node);											// Destroys a node, and its data.
	bool DestroyList();														// Destroys the entire list, including the node's data.
	/* Node Retrival */
	Node *GetHead();														// Returns the head node.
	Node *GetTail();														// Returns the tail node.
	Node *GetCurrent();														// Returns the current node.
	//virtual GetNode(unsigned int index);
	/* Movement */
	void GotoHead();														// Sets current to head.
	void GotoTail();														// Sets current to tail.
	void SetCurrent(Node *newCurrentNode);									// Sets a node to the current.
	void TraverseDown();													// Traverses Down the list.
	/* Add / Remove */
	virtual void AddNodeAtBeg(Node &node);									// Add node at beginning.
	virtual void AddNodeAtEnd(Node &node);									// Add node at end.
	virtual void RemoveNodeAtBeg();											// Remove node at beginning.
	virtual void RemoveNodeAtEnd();											// Remove node at end.
	virtual void AddNodeAfterNode(Node &node, Node &MarkerNode);			// Add a node after another marker node.
	virtual void RemoveNodeAfterNode(Node &MarkerNode); // Not Tested		// Remove a node after another marker node.
	//virtual bool AddNodeAtIndex(Node &node, unsigned int index);	
	unsigned int GetNumberOfNodes();
	

	/* File IO */
	bool SaveToFile(char filename[]);
	bool OpenFromFile(char filename[], const unsigned int DataSize);
	bool SaveAddressesToFile(char filename[]);
	bool OpenAddressesFromFile(char filename[]);

#ifdef __DEBUG_LIST__
	/*For debugging purposes*/
	void MemoryDumpOfNode(Node &node);
	void MemoryDumpOfList();
#endif

protected:

	Node *head,
		 *tail,
		 *current;
		
	
	unsigned int NumberOfNodes;

}SingleList;
