#pragma once
/*		CircularDList.h
 *
 *		Circular doubly linked list data structure.
 *		Coded by Joseph A. Marrero
 *		4/1/04
 */
#include "dlist.h"

#ifdef __DEBUG_CIRCULARDLIST__
	#ifndef __DEBUG_NODE__
	#define __DEBUG_NODE__
	#endif
#endif

class CircularDList :
	public DList
{
public:
	CircularDList(void);
	virtual ~CircularDList(void);
	/*Add / Remove */
	void AddNodeAtBeg(Node &node);
	void AddNodeAtEnd(Node &node);
	void RemoveNodeAtBeg();
	void RemoveNodeAtEnd();
};
