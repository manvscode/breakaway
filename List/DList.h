#pragma once
/*		DList.h
 *
 *		Doubly linked list data structure.
 *		Coded by Joseph A. Marrero
 *		4/1/04
 */
#include "list.h"

#ifdef __DEBUG_DLIST__
	#ifndef __DEBUG_NODE__
	#define __DEBUG_NODE__
	#endif
#endif

typedef class DList :
	public List
{
private:
	
public:
	DList(void);
	DList(void (*DestroyNodeData)(void *Data));
	virtual ~DList(void);
	/* Movement */
	void TraverseUp();													// Traverses Up the list.
	/*Add / Remove */
	virtual void AddNodeAtBeg(Node &node);								// Add node at the beginning.
	virtual void AddNodeAtEnd(Node &node);								// Add node at the end.
	virtual void RemoveNodeAtBeg();										// Remove node from beginning.
	virtual void RemoveNodeAtEnd();										// Remove node from the end.
	virtual void AddNodeAfterNode(Node &node, Node &MarkerNode);		// Add node after a marker node.
	virtual void RemoveNodeAfterNode(Node &MarkerNode);					// Remove node after a marker node.




}DoubleList;
