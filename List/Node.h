#pragma once
/*		Node.h
 *
 *		A general node object for data structures.
 *
 *		Coded by Joseph A. Marrero
 *		4/1/04
 */
#include "List.h"

#ifndef NULL
	#define NULL 0
#endif
	
typedef class Node
{
	friend class List;
	friend class DList;
	friend class CircularList;
	friend class CircularDList;
protected:
	void (*Destroy_Data)(void *data);
private:
	

	unsigned int DataSize;
	void *Data;
	
	Node *prev;
	Node *next;
	//typedef prev left;
	//typedef next right;

	unsigned int NodeIndex;

public:
	Node(void);
	Node(void *Data, unsigned int DataSize);
	bool SetData(void *Data, unsigned int Size);

	inline void *GetData()
	{
		return Data;
	}
	inline Node *GetNext()
	{
		return next;
	}
	inline Node *GetPrev()
	{
		return prev;
	}

	inline unsigned int GetSize()
	{
		return DataSize;
	}
	inline void DestroyData()
	{
		if(Data != NULL){
			//delete Data;  //Does not work
			(*Destroy_Data)(Data);
			//List::DestroyNodeData;
			Data = NULL;
		}
	}
	~Node(void);
}Link;
