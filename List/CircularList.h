#pragma once
/*		CircularList.h
 *
 *		Circular linked list data structure.
 *		Coded by Joseph A. Marrero
 *		4/1/04
 */
#include "list.h"

#ifdef __DEBUG_CIRCULARLIST__
	#ifndef __DEBUG_NODE__
	#define __DEBUG_NODE__
	#endif
#endif

class CircularList :
	public List
{
public:
	CircularList(void);
	virtual ~CircularList(void);
	/*Add / Remove */
	void AddNodeAtBeg(Node &node);
	void AddNodeAtEnd(Node &node);
	void RemoveNodeAtBeg();
	void RemoveNodeAtEnd();


};
