

#include "block.h"

Block::Block(int x_pos, int y_pos)
{
	x = x_pos;
	y = y_pos;
	width = 93;
	height = 22;

	collide_width = (int)(width * 0.9);
	collide_height = (int)(height * 0.9);

	collide_x_offset = (width - collide_width) >> 1;
	collide_y_offset = (height - collide_height) >> 1;
	base = 0;
	Init();
}

Block::~Block()
{
	DeInit();
}
void Block::DrawBlackOutline()
{
	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glLineWidth(2);

		glVertex2i(0, 0);
		glVertex2i((GLint) width, 0);

		glVertex2i(0, 0);
		glVertex2i(0, (GLint) height);

		glVertex2i((GLint) width, 0);
		glVertex2i((GLint) width, (GLint) height);

		glVertex2i((GLint) width, (GLint) height);
		glVertex2i(0, (GLint) height);
	glEnd();
}
//Renders relative to bottom life
void Block::Draw()
{
	glPushMatrix();
		glTranslatef((GLfloat) x , (GLfloat) y, 0.0f);
		glCallList(base);
	glPopMatrix();
    
}
void Block::Init()
{
	base = glGenLists(1);
	
	glNewList(base, GL_COMPILE);
		glBegin(GL_QUADS);

			glTexCoord2i(0, 0); glVertex2i(0, 0);
			glTexCoord2i(1, 0); glVertex2i((GLint) width, 0);
			glTexCoord2i(1, 1); glVertex2i((GLint) width, (GLint) height);
			glTexCoord2i(0, 1); glVertex2i(0, (GLint) height);
		glEnd();
	glEndList();
}
void Block::DeInit()
{
	glDeleteLists(base, 1);
}