
#include <stdio.h>
#include <gl/glaux.h>
#include <mmsystem.h>
#include "scene.h"
#include ".\scene.h"


#define ENERGY_LOSS		25


Scene::Scene(Game *game_Instance)
{
	
	gameInstance = game_Instance;
	Score_Manager = game_Instance->Score_Manager;
	
	User_Block = new UserBlock(392, 50);
	ball = new Ball(150, 400);

	
	Wall.width = 740;
	Wall.height = 515;
	Wall.x = 22;
	Wall.y = 20;

	cloud_mx = 800;
	cloud_my = 600;

}
Scene::~Scene(void)
{
	delete User_Block;
	delete ball;
}
bool Scene::CreateBlockList()
{
	register int yDistance = BlockDistance;

	for(register int BlockRow = 0; BlockRow < 6; BlockRow++){ // 6
		for(register int BlockCol = 0; BlockCol < 8; BlockCol++){ // 8
			Block *newBlock = new Block(BlockWallSpace + ((BlockCol  << 6) + (BlockCol << 4) + (BlockCol << 3) + (BlockCol << 2)  + 1), yDistance);
			BlockList.push_front( newBlock );
		}
		yDistance -= 22; // 22 = BlockHeight
	}

	return true;
}

void Scene::DestroyBlockList()
{
	while( BlockList.size( ) > 0 )
	{
		delete BlockList.front( );
		BlockList.erase( BlockList.begin( ) );
	}
}

void Scene::RenderScene()
{
	static Texture *currentTexture = NULL;

	currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_SCENEBG_TEXTURE);
	
	// Draw Background
	glDisable( GL_BLEND );
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, (GLfloat) GL_DECAL);
	glBindTexture(GL_TEXTURE_2D, currentTexture->TextureName);
	glCallList(BGTextureName);
	glEnable( GL_BLEND );
	

	//Check for wall collision
	if(ball->x > Wall.x + Wall.width){
		ball->x = Wall.x + Wall.width;
		ball->NegateXVelocity();
		ball->BallVelocityX -= ENERGY_LOSS;
	}
	else if(ball->x < Wall.x){
		ball->x = Wall.x;
		ball->NegateXVelocity();
		ball->BallVelocityX -= ENERGY_LOSS;
	}
	else if( ball->y > Wall.y + Wall.height ){
		ball->y = Wall.y + Wall.height;
		ball->NegateYVelocity();
		ball->BallVelocityY -= ENERGY_LOSS;
	}
	
	else if(ball->y < Wall.y && gameInstance->GameState != 2){
		ball->y = Wall.y;
		gameInstance->SetGameOverState();
		return;
	}
		
	//Check for Collision with Ball and UserBlock
	if(Sprite_Collide(ball, User_Block) == TRUE ) {
		// from  the left
		if(ball->x < User_Block->x){
			ball->x = User_Block->x;
			ball->NegateXVelocity();
			ball->BallVelocityX -= ENERGY_LOSS;
		}
		// from the right
		else if(ball->x > User_Block->x + User_Block->width){
			ball->x = User_Block->x + User_Block->width;
			ball->NegateXVelocity();
			ball->BallVelocityX -= ENERGY_LOSS;
		}
		// from the top
		else if( ball->y < User_Block->y + User_Block->height && ball->y > User_Block->y ){
			ball->y = User_Block->y + User_Block->height;
			ball->NegateYVelocity();
			ball->BallVelocityY -= ENERGY_LOSS;
		}
		else {
			ball->y = User_Block->y -  ball->height;
			ball->NegateYVelocity();
			ball->BallVelocityY -= ENERGY_LOSS;
		}

	}

	glBindTexture(GL_TEXTURE_2D, GAME_BLOCK_TEXTURE);

	// Draw User Block
	HandleUserControl();
	User_Block->Draw();


	if(BlockList.size() == 0 && gameInstance->GameState != 4){
		gameInstance->SetGameNextLevelState();
		ball->SetBallXVelocity(25);
		ball->SetBallXVelocity(25);
		return;
	}

	//Draw Blocks in list
	list<Block *>::iterator itr;

	for( itr = BlockList.begin(); itr != BlockList.end(); ++itr )
	{
		Block *currentBlock = *itr;

		//Check for Collision with Ball and 1st Block
		if(Sprite_Collide(ball, currentBlock) == TRUE)
		{
			sndPlaySound( "hit.wav", SND_FILENAME | SND_ASYNC  );

			// from  the left
			if(ball->x < currentBlock->x ){
				ball->x = currentBlock->x;
				ball->NegateXVelocity();
				ball->BallVelocityX -= ENERGY_LOSS;
			}
			// from the right
			else if(ball->x > currentBlock->x + currentBlock->width){
				ball->x = currentBlock->x + currentBlock->width;
				ball->NegateXVelocity();
				ball->BallVelocityX -= ENERGY_LOSS;
			}
			// from the top
			else if( ball->y < currentBlock->y + currentBlock->height && ball->y > currentBlock->y ){
				ball->y = currentBlock->y + currentBlock->height;
				ball->NegateYVelocity();
				ball->BallVelocityY -= ENERGY_LOSS;
			}
			// from the bottom
			else {
				ball->y = currentBlock->y -  ball->height;
				ball->NegateYVelocity();
				ball->BallVelocityY -= ENERGY_LOSS;
			}
			
			delete currentBlock;
			BlockList.erase( itr );
			
			break;
		}
		currentBlock->Draw();
	}


	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	
	glEnable( GL_BLEND );
	// Draw Ball
	glBindTexture(GL_TEXTURE_2D, GAME_BALL_TEXTURE);
	ball->Draw();
	ball->MoveBall();

	if( cloud_mx >= 0 && cloud_mx <= 800 ){
		glBindTexture(GL_TEXTURE_2D, GAME_CLOUD_TEXTURE);
		glColor4f(1.0f,1.0f,1.0f, 0.5f);
		//if(cloud_mx > 0) 
			DrawClouds((float) cloud_mx - 800, 0.0f);

			DrawClouds((float) cloud_mx, 0.0f);
		cloud_mx += 185 * gameInstance->getTimeStep();
	}
	else{
		cloud_mx = 0;
	}

	if( cloud_my >= 0 && cloud_my <= 600 ){
		glBindTexture(GL_TEXTURE_2D, GAME_CLOUD_TEXTURE);
		glColor4f(1.0f,1.0f,1.0f, 0.10f);
		/*if(cloud_my > 0)*/ 
			DrawClouds( 0.0f, (float) cloud_my-600);
		DrawClouds(0.0f, (float) cloud_my);
		cloud_my += 185 * gameInstance->getTimeStep();
	}
	else{
		cloud_my = 0;
	}
	glDisable( GL_BLEND );
	
	glDisable(GL_TEXTURE_2D);
}
void Scene::SceneInit(void)
{

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	ball->SetGameInstance(gameInstance);
	ball->SeedRandomLocation();
	BGTextureName = glGenLists(2);
	CloudTexture = BGTextureName + 1;
	
	glNewList(BGTextureName, GL_COMPILE);
		glBegin(GL_QUADS);
				glTexCoord2i(0, 0); glVertex2i(0, 0);
				glTexCoord2i(1, 0); glVertex2i(800, 0);
				glTexCoord2i(1, 1); glVertex2i(800, 600);
				glTexCoord2i(0, 1); glVertex2i(0, 600);
			glEnd();
	glEndList();

	glNewList(CloudTexture, GL_COMPILE);
		glBegin(GL_QUADS);
			glTexCoord2i(0, 0); glVertex2i(0, 0);
			glTexCoord2i(1, 0); glVertex2i(800, 0);
			glTexCoord2i(1, 1); glVertex2i(800, 600);
			glTexCoord2i(0, 1); glVertex2i(0, 600);
		glEnd();
	glEndList();

}
void Scene::HandleUserControl()
{
	static int x_Position = User_Block->x;

	if( gameInstance->keys[VK_LEFT] == TRUE){
		x_Position -= 800 * gameInstance->getTimeStep();//User_Block
		
		if(x_Position < 27)
			x_Position = 27;
	}
	else if( gameInstance->keys[VK_RIGHT] == TRUE){
		x_Position += 800 * gameInstance->getTimeStep(); //Do something
		
		if(x_Position > 660)
			x_Position = 660;
	}

	User_Block->SetMove(x_Position);
}
void Scene::SetBallPosition(int x, int y)
{
	ball->SetPosition(x, y);
}
void Scene::AdjustBallVelocity(int velocity)
{
	ball->AdjustYVelocity((float) velocity);
}
short int Scene::Sprite_Collide(SpriteObj *object1, SpriteObj *object2)
{
    int left1, left2;
    int right1, right2;
    int top1, top2;
    int bottom1, bottom2;


	left1 = object1->x + object1->collide_x_offset;
	left2 = object2->x + object2->collide_x_offset;
	right1 = left1 + object1->collide_width;
	right2 = left2 + object2->collide_width;
	top1 = object1->y + object1->collide_y_offset;
	top2 = object2->y + object1->collide_y_offset;
	bottom1 = top1 + object1->collide_height;
	bottom2 = top2 + object2->collide_height;

    if (bottom1 < top2) return 0;
    if (top1 > bottom2) return 0;
  
    if (right1 < left2) return 0;
    if (left1 > right2) return 0;

    return	1;


}

void DestroyBlock(void *data)
{
	delete (Block *) data;
}
void Scene::TextureTest()
{
}
void Scene::DrawClouds(float x, float y)
{

	glPushMatrix();
		glTranslatef(x, y, 0.0f);
		glCallList(CloudTexture);
	glPopMatrix();


}
