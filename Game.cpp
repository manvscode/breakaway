/*	Game.cpp
 *
 *	Manages game's states and other managers.
 *	Coded by Joseph A. Marrero
 */
#include <stdio.h>
#include "game.h"


Game::Game(void)
{
	hDC = NULL;
	GameState = GAME_START;			//Display UI
#ifdef __SHOW_FPS__
	lastUpdate = 0;
	fpsUpdateInterval = 0.85f;		// 0.5
	numFrames = 0;
	fps = 0;
#endif

	for(register int i = 0; i < 256; i++)
		keys[i] = FALSE;

	GameUserInterface = NULL;
	GameScene = NULL;
	Texture_Manager = NULL;
	GameTimer = NULL;
	Text_Out = NULL;
	Score_Manager = NULL;
	timeStep = 0.0f;
}

Game::~Game(void)
{
	DeInitGame();
}
void Game::SetHDC(HDC g_hDC)
{
	hDC = g_hDC;
}
bool Game::InitGame()
{
	// OpenGL initialization...

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	//glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_FASTEST);
	glHint(GL_LINE_SMOOTH_HINT, GL_FASTEST);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_FASTEST);


	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	//glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);							// Depth Buffer Setup
	//glEnable( GL_ALPHA_TEST );
	glFrontFace(GL_CCW);
	
	glPixelStorei( GL_PACK_ALIGNMENT, 4);
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4);


	if( (GameTimer = new Timer) == NULL ){
		MessageBox(NULL, "Unable to create a timer!", "Error", MB_OK | MB_ICONERROR);	
		return false;	
	}
	else
		GameTimer->ResetTimer();
	if( (GameUserInterface = new GameUI(this)) == NULL){
		MessageBox(NULL, "Unable to create GameUI!", "Error", MB_OK | MB_ICONERROR);	
		return false;
	}
	if( (Text_Out = new TextRasterizer(hDC)) == NULL){
		MessageBox(NULL, "Unable to create TextRasterizer!", "Error", MB_OK | MB_ICONERROR);
		return false;
	}
	else{
		Text_Out->CreateBitmapFont("Terminator Two", 28);
	}
#ifdef __SHOW_FPS__
	if( (FPS_Text = new TextRasterizer(hDC) ) == NULL){
		MessageBox(NULL, "Unable to create TextRasterizer for FPS!", "Error", MB_OK | MB_ICONERROR);
		return false;
	}
	else{
		FPS_Text->CreateBitmapFont("Tahoma", 12);
	}
#endif

	if( (Score_Manager = new ScoreManager(this)) == NULL){
		MessageBox(NULL, "Unable to create ScoreManager!", "Error", MB_OK | MB_ICONERROR);
		return false;
	}
	if( (Texture_Manager = new TextureManager) == NULL){
		MessageBox(NULL, "Unable to create TextureManager!", "Error", MB_OK | MB_ICONERROR);
		return false;
	}
	else {
		Texture_Manager->AddTGATexture("Textures\\title.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\button01.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\button02.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\selector.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\scenebg.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\block.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\ball.tga", 2);
		Texture_Manager->AddTGATexture("Textures\\clouds.tga", 2);
	}
	if((GameScene = new Scene(this)) == NULL){
		MessageBox(NULL, "Unable to create GameScene!", "Error", MB_OK | MB_ICONERROR);
		return false;
	}
	Text_Out->ClearFont();
	Text_Out->CreateBitmapFont("Arial Black", 28);

	sndPlaySound( "background.wav", SND_LOOP | SND_FILENAME | SND_ASYNC | SND_NODEFAULT );

	GameScene->SceneInit();
	return true;
}
void Game::DeInitGame()
{
	sndPlaySound( NULL, 0 );

	delete GameTimer;
	delete GameUserInterface;
	delete GameScene;
	delete Texture_Manager;
	delete Score_Manager;
	delete Text_Out;
#ifdef __SHOW_FPS__
	delete FPS_Text;
#endif
	
}
void Game::SetGameStartState()
{
	GameState = GAME_START;
}
void Game::SetInGameState()
{
	GameState = GAME_INGAME;
}
void Game::SetGameOverState()
{
	GameState = GAME_GAMEOVER;
}
void Game::SetGameNextLevelState()
{
	GameState = GAME_NEXTLEVEL;
}
void Game::Render()
{
	glDisable(GL_TEXTURE_2D);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//sndPlaySound( "background.wav", SND_LOOP | SND_FILENAME | SND_ASYNC  );
	

	switch(GameState){
		case GAME_START:
			
			GameUserInterface->DisplayMenuInterface();
			break;
		case GAME_INGAME:
			GameScene->RenderScene();

#ifdef __SHOW_FPS__
			glColor3f(1.0f,1.0f,0.3f);
			UpdateFPS();
			FPS_Text->PrintString( 15, 3, fps_string);
#endif
			glColor3f(1.0f,1.0f,1.0f);
			Score_Manager->WriteScore();
						
			break;
		case GAME_GAMEOVER:

			#ifdef __SHOW_FPS__
				numFrames = 0;
			#endif
			GameScene->InitClouds();
			Score_Manager->ResetScore();
			//PlaySound( "game_over.wav", NULL, SND_FILENAME | SND_ASYNC );
			sndPlaySound( "game_over.wav", SND_FILENAME | SND_ASYNC | SND_NODEFAULT );
			Text_Out->PrintString(350, 300, "Game Over!");
			SwapBuffers(hDC);
			GameTimer->Wait(2);

			GameScene->getBall()->SeedRandomLocation();

			
			GameScene->DestroyBlockList();
			//GameScene->SetBallPosition(392, 400);
			SetGameStartState();
			break;
		case GAME_WIN:
			#ifdef __SHOW_FPS__
				numFrames = 0;
			#endif
			GameScene->InitClouds();
			break;
		case GAME_NEXTLEVEL:
			#ifdef __SHOW_FPS__
				numFrames = 0;
			#endif
			GameScene->InitClouds();
			GameTimer->ResetTimer();
			//PlaySound( "next_level.wav", NULL, SND_FILENAME | SND_ASYNC );
			sndPlaySound( "next_level.wav", SND_FILENAME | SND_ASYNC | SND_NODEFAULT  );
			
			Text_Out->PrintString(350,300,"Next Level");
			SwapBuffers(hDC);
			GameTimer->Wait(2);
			GameScene->DestroyBlockList();	// potentially not needed!
			GameScene->CreateBlockList();
			//GameScene->SetBallPosition(392, 400);
			GameScene->AdjustBallVelocity(11);
			SetInGameState();
			GameTimer->ResetTimer();
		
			return;
			break;
		default:
			break;
	}

	

	SwapBuffers(hDC);
}
Game* Game::instance = NULL;
Game* Game::GetInstance()
{
	if(instance == NULL){
		instance = new Game;
		return instance;
	}
	else
		return instance;
}

#ifdef __SHOW_FPS__
void Game::UpdateFPS()
{
	numFrames++;

	float currentUpdate = GameTimer->GetTime();

	if( currentUpdate - lastUpdate >= fpsUpdateInterval )
	{
		fps = numFrames / (currentUpdate - lastUpdate);
		lastUpdate = currentUpdate;
		numFrames = 0;
	}

	sprintf( fps_string, "%0.1f fps", fps );
}
#endif
