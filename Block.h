#pragma once
#include <windows.h>
#include <gl/gl.h>
#include "SpriteObj.h"

class Block : public SpriteObj
{
	friend class Scene;
private:
	unsigned int base;
	
public:
	Block(int x_pos, int y_pos);
	~Block();
	void DrawBlackOutline();
	void Draw();
	void Init();
	void DeInit();
	
};
