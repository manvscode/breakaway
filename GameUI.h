#pragma once
/*	GameUI.h
 *
 *	Manages game's user interface.
 *	Coded by Joseph A. Marrero
 */
#include "game.h"

/*** Textures ***/
#define GAME_OPENING_BG			1
#define GAME_MENU_BUTTON01		2
#define GAME_MENU_BUTTON02		3
#define GAME_MENU_SELECTOR		4



class GameUI
{
	friend class Game;
private:
	Game *gameInstance;

	unsigned int Selection;
	unsigned int Choice;

public:
	GameUI(Game *game_Instance);
	~GameUI();
	void DisplayMenuInterface();
};
