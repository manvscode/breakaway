/*	TextRasterizer.h
 *
 *	Writes Text to a GL context.
 *	Coded by Joseph A. Marrero
 *
 */
#include <stdio.h>
#include "TextRasterizer.h"

TextRasterizer::TextRasterizer(HDC hDC)
{
	m_HDC = hDC;
}

TextRasterizer::~TextRasterizer(void)
{
	DeleteObject(hFont);
	ClearFont();
}
BOOL TextRasterizer::CreateBitmapFont(char *fontName, int fontSize)
{
	

	if( (base = glGenLists(96)) == 0 ){  // 96 chars
		MessageBox(NULL, "Unable to create a display list for text!", "Error", MB_OK | MB_ICONERROR);	
		return FALSE;
	}
	

	if( stricmp( fontName, "symbol") == 0)
	{
		hFont = CreateFont(fontSize, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE,
						   SYMBOL_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS,
						   ANTIALIASED_QUALITY, FF_DONTCARE | DEFAULT_PITCH,
						   fontName);
	}
	else {
		hFont = CreateFont(fontSize, 0,0,0, FW_BOLD, FALSE, FALSE, FALSE,
						   ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS,
						   ANTIALIASED_QUALITY, FF_DONTCARE | DEFAULT_PITCH,
						   fontName);
	}

	if( !hFont ){
		MessageBox(NULL, "Unable to create a font object for text!", "Error", MB_OK | MB_ICONERROR);
		return FALSE;
	}
	SelectObject(m_HDC, hFont);
	wglUseFontBitmaps(m_HDC, 32, 96, base);
	

	//return base;
	return TRUE;
}
void TextRasterizer::PrintString(char *String)
{
	if( (base == 0) || (String == NULL))
		return;

	glPushAttrib(GL_LIST_BIT);
		glListBase(base - 32);
		glCallLists( (GLsizei) strlen(String), GL_UNSIGNED_BYTE, String);
	glPopAttrib();
}
void TextRasterizer::PrintString(float x, float y, char *String)
{
	if( (base == 0) || (String == NULL))
		return;

	//glPushMatrix();
		//glTranslatef(x, y, 0);
		glPushAttrib(GL_LIST_BIT);
			glRasterPos2f( (GLfloat) x, (GLfloat) y);
		
			glListBase(base - 32);		
			glCallLists( (GLsizei) strlen(String), GL_UNSIGNED_BYTE, String);
		glPopAttrib();
	//glPopMatrix();
}
void TextRasterizer::ClearFont()
{
	if(base != 0)
		glDeleteLists(base, 96);
}
