/*	GameUI.cpp
 *
 *	Manages game's user interface.
 *	Coded by Joseph A. Marrero
 */
#include "gameui.h"


GameUI::GameUI(Game *game_Instance)
{
	gameInstance = game_Instance;
	Selection = 0;
}

GameUI::~GameUI()
{
}
void GameUI::DisplayMenuInterface()
{
	glEnable( GL_BLEND );
	static float yMover = 129.0f;
	
	Texture *currentTexture = NULL;

	glRasterPos2i(0, 0);
	currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_OPENING_BG);
	glPixelZoom((GLfloat) 800.0 / currentTexture->width,
				(GLfloat) 600.0 / currentTexture->height);

	glDrawPixels( currentTexture->width,
				  currentTexture->height, 
				  currentTexture->format,
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) currentTexture->texels );

	glRasterPos2i(571, 193);
	glPixelZoom(1.0f,1.0f);
	currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_MENU_BUTTON01);
	glDrawPixels( currentTexture->width,
				  currentTexture->height, 
				  currentTexture->format,
				  GL_UNSIGNED_BYTE, 
				  (GLubyte *) currentTexture->texels );
	glRasterPos2f(571.0f, 129.0f);
	currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_MENU_BUTTON02);
	glDrawPixels( currentTexture->width,
				  currentTexture->height, 
				  currentTexture->format,
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) currentTexture->texels );
	
	if(gameInstance->keys[VK_UP] == TRUE)
		Selection = 1;
	else if(gameInstance->keys[VK_DOWN] == TRUE)
		Selection = 2;

	if( Selection == 1){
			
			glRasterPos2f(520.0f, yMover);
			currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_MENU_SELECTOR);
			glDrawPixels( currentTexture->width,
						  currentTexture->height, 
						  currentTexture->format,
						  GL_UNSIGNED_BYTE,
						  (GLubyte *) currentTexture->texels );

			if( yMover <= 193.0f  )
				yMover += 350.8f * gameInstance->getTimeStep();
	}
	else if(Selection == 2){
			glRasterPos2i(520.0f, yMover);
			currentTexture = gameInstance->Texture_Manager->GetTGATexture(GAME_MENU_SELECTOR);
			
			glDrawPixels( currentTexture->width,
						  currentTexture->height, 
						  currentTexture->format,
						  GL_UNSIGNED_BYTE,
						  (GLubyte *) currentTexture->texels );
			


			if( yMover >= 129.0f ) yMover -= 350.8f * gameInstance->getTimeStep();
	}

	glDisable( GL_BLEND );
	if( gameInstance->keys[VK_RETURN] == TRUE){
		switch(Selection){
			case 1:
				gameInstance->GameScene->SceneInit();
				gameInstance->GameScene->DestroyBlockList();
				gameInstance->GameScene->CreateBlockList();
				//gameInstance->GameScene->SetBallPosition(392, 400);
				gameInstance->SetInGameState();
				gameInstance->GameTimer->ResetTimer();
				break;
			case 2:
				PostQuitMessage(0);
				break;
			default:
				break;
		}
	}

	
}